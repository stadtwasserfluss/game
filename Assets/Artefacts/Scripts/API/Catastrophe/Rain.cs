using System;
using System.Collections.Generic;
using Artefacts.Scripts.API.Gameplay;
using Artefacts.Scripts.API.Map;
using Artefacts.Scripts.API.Notification;

namespace Artefacts.Scripts.API.Catastrophe {
    public class Rain {
        private readonly GameTime _gt;

        public Rain(int index) {
            var gm = GameManager.I;
            var currentLevel = gm.CurrentLevel;
            this._gt = GameTime.I;

            this.PrewarnTime = currentLevel.PreparationTime;
            this.StartAt = this._gt.Now + currentLevel.EventAt;
            this.Duration = currentLevel.EventDuration;
            this.AffectedObjects = new List<Sector>();
            this.Index = index;

            var evtmgr = EventManager.I;

            evtmgr.AddNotification(new CatastropheNotification(this));
            evtmgr.AddNotification(new CatastrophePassedNotification(this));
        }

        public List<Sector> AffectedObjects { get; }
        public long PrewarnTime { get; }
        public long StartAt { get; }
        public long Duration { get; }
        public long WarnAt => this.StartAt - this.PrewarnTime;
        public long TimeLeft => Math.Max(this.StartAt - this._gt.Now, 0);
        public long EndAt => this.StartAt + this.Duration - 1;
        public long DurationLeft => Math.Max(this.EndAt - this._gt.Now, 0);
        public int Index { get; }
        public bool IsAffecting => GameTime.I.Now >= this.StartAt && GameTime.I.Now <= this.EndAt;

        public void AddAffectedObjects(IEnumerable<Sector> sectors) {
            this.AffectedObjects.AddRange(sectors);
        }
    }
}