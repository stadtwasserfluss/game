using Artefacts.Scripts.API.Action;
using Artefacts.Scripts.API.Communication;
using Artefacts.Scripts.API.Gameplay;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API {
    public static class ApiController {
        public static void ResetLazySingletons() {
            ActionList.Reset();
            StoryProvider.Reset();
            BankAccount.Reset();
            EventManager.Reset();
            GameManager.Reset();
            GameTime.Reset();
            SectorConfigurationList.Reset();
        }
    }
}