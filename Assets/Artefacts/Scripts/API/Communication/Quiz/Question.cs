﻿// ReSharper disable CollectionNeverUpdated.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable ClassNeverInstantiated.Global

using System.Collections.Generic;

namespace Artefacts.Scripts.API.Communication.Quiz {
    public class Question {
#if UNITY_WEBGL
        public int Id { get; set; }
        public string When { get; set; }
        public string Text { get; set; }
        public List<Answer> Answers { get; set; }
        public bool IsAnswered { get; set; } = false;
#else
        public int Id { get; private set; }
        public string When { get; private set; }
        public string Text { get; private set; }
        public List<Answer> Answers { get; private set; }
        public bool IsAnswered { get; set; } = false;
#endif

        public class Answer {
#if UNITY_WEBGL
            public string Text { get; set; }
            public bool IsCorrect { get; set; }
#else
            public string Text { get; private set; }
            public bool IsCorrect { get; private set; }
#endif
        }
    }
}