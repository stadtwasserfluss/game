using Artefacts.Scripts.API.Gameplay;

namespace Artefacts.Scripts.API.Communication.Quiz {
    public class Quizmaster {
        private Question _currentQuestion;

        public int CorrectAnswers { get; private set; }
        public int TotalAnswers { get; private set; }

        public Question CurrentQuestion =>
            this._currentQuestion ?? (this._currentQuestion = GameManager.I.CurrentStory?.NextQuestion);

        public bool SetAnswer(Question.Answer answer) {
            var gm = GameManager.I;

            this._currentQuestion.IsAnswered = true;
            this.TotalAnswers++;

            if (!this._currentQuestion.Answers.Contains(answer) || !answer.IsCorrect) {
                gm.CurrentStory.Notify($"question-{this._currentQuestion.Id}-incorrect");
                gm.CurrentLevel.DecreasePublicAwarenessBy(gm.CurrentLevel.PublicAwarenessConfiguration.QuizBonus);
                return false;
            }

            this.CorrectAnswers++;
            gm.CurrentStory.Notify($"question-{this._currentQuestion.Id}-correct");
            gm.CurrentLevel.RaisePublicAwarenessBy(gm.CurrentLevel.PublicAwarenessConfiguration.QuizBonus);
            return true;
        }

        public void NextQuestion() {
            if (!this._currentQuestion.IsAnswered) return;

            this._currentQuestion = null;
        }
    }
}