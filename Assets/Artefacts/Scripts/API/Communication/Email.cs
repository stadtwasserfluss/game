// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable ClassNeverInstantiated.Global

namespace Artefacts.Scripts.API.Communication {
    public class Email : Message {
#if UNITY_WEBGL
        public int Id { get; set; }
        public string FromAdress { get; set; }
        public string Subject { get; set; }
        public bool Read { get; set; }
#else
        public int Id { get; private set; }
        public string FromAdress { get; private set; }
        public string Subject { get; private set; }
        public bool Read { get; set; }
#endif
    }
}