// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Artefacts.Scripts.API.Communication {
    public class Message {
#if UNITY_WEBGL
        public string When { get; set; } = null;
        public int Delay { get; set; } = 0;
        public string From { get; set; }
        public string Text { get; set; }
        public bool Pause { get; set; } = false;
#else
        public string When { get; protected set; } = null;
        public int Delay { get; protected set; } = 0;
        public string From { get; protected set; }
        public string Text { get; protected set; }
        public bool Pause { get; protected set; } = false;
#endif
    }
}