#if UNITY_WEBGL
using Artefacts.Scripts.API.Yaml.WebGl.Story;
#else
using Artefacts.Scripts.API.Yaml;
#endif
using System;
using System.Collections.Generic;
using Artefacts.Scripts.API.Gameplay;

namespace Artefacts.Scripts.API.Communication {
    public class StoryProvider {
        private static Lazy<StoryProvider> _lazyInstance = StoryProvider.GetLazyInstance();
        private Dictionary<int, Story> _stories;

        private StoryProvider() {
            this.Initialize();
        }

        public static StoryProvider Instance => StoryProvider._lazyInstance.Value;

        public Story GetForLevel(Level level) {
            this._stories.TryGetValue(level.Id, out var story);
            return story;
        }

        public static void Reset() {
            StoryProvider._lazyInstance = StoryProvider.GetLazyInstance();
        }

        private static Lazy<StoryProvider> GetLazyInstance() {
            return new Lazy<StoryProvider>(() => new StoryProvider());
        }

        private void Initialize() {
#if UNITY_WEBGL
            this._stories = new Dictionary<int, Story> {
                {1, new Level01Story()},
                {2, new Level02Story()},
                {3, new Level03Story()}
            };
#else
            this._stories = new Dictionary<int, Story>();
            foreach (string fileName in FileLayer.GetYamlFilesInDirectory("data/story")) {
                var s = YamlLoader<Story>.Load(fileName);
                this._stories.Add(s.Level, s);
            }
#endif
        }
    }
}