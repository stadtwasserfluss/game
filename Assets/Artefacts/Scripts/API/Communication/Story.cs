// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedMember.Local

using System.Collections.Generic;
using Artefacts.Scripts.API.Communication.Quiz;
using Artefacts.Scripts.API.Gameplay;
using UnityEngine;

namespace Artefacts.Scripts.API.Communication {
    public class Story {
        private readonly Queue<Email> _allEmails = new Queue<Email>();
        private readonly Queue<Message> _allMessages = new Queue<Message>();

        private readonly Queue<Question> _allQuestions = new Queue<Question>();
        private readonly Dictionary<Email, long> _delayedEmails = new Dictionary<Email, long>();
        private readonly Dictionary<Message, long> _delayedMessages = new Dictionary<Message, long>();
        private readonly Queue<Email> _nextEmails = new Queue<Email>();
        private readonly Queue<Message> _nextMessages = new Queue<Message>();
        private readonly Queue<Question> _nextQuestions = new Queue<Question>();
        private readonly List<string> _receivedEvents = new List<string>();

        private long _nextDelayedEmailAt;
        private long _nextDelayedMessageAt;

        public int Level { get; protected set; }

        public List<Message> Messages {
            get => null;
            protected set {
                foreach (var message in value) this._allMessages.Enqueue(message);
            }
        }

        public List<Email> Emails {
            get => null;
            protected set {
                foreach (var email in value) this._allEmails.Enqueue(email);
            }
        }

        public List<Question> Questions {
            get => null;
            protected set {
                foreach (var question in value) this._allQuestions.Enqueue(question);
            }
        }

        public Message NextMessage {
            get {
                this.Update(this._allMessages, ref this._nextDelayedMessageAt, this._nextMessages);
                return this._nextMessages.Count == 0 ? null : this._nextMessages.Dequeue();
            }
        }

        public Email NextEmail {
            get {
                this.Update(this._allEmails, ref this._nextDelayedEmailAt, this._nextEmails);
                return this._nextEmails.Count == 0 ? null : this._nextEmails.Dequeue();
            }
        }

        public Question NextQuestion {
            get {
                if (this._allQuestions.Count == 0)
                    return null;

                var candidate = this._allQuestions.Peek();

                if (!this._receivedEvents.Contains(candidate.When) && !(candidate.When is null))
                    return null;

                this._receivedEvents.Remove(candidate.When);

                return this._allQuestions.Dequeue();
            }
        }

        public void Notify(string evt) {
            if (this._receivedEvents.Contains(evt))
                return;

            this._receivedEvents.Add(evt);
        }

        private void Update<T>(Queue<T> all, ref long nextDelayedAt, Queue<T> next) where T : Message {
            if (all.IsEmpty())
                return;

            var candidate = all.Peek();

            if (candidate.Delay > 0) {
                if (nextDelayedAt < 0) {
                    nextDelayedAt = (GameTime.I.IsPaused && candidate.Pause
                                         ? (long) Time.fixedTime
                                         : GameTime.I.Now) +
                                    candidate.Delay;
                    return;
                }

                if ((GameTime.I.IsPaused && candidate.Pause ? (long) Time.fixedTime : GameTime.I.Now) <
                    nextDelayedAt) return;
            }

            nextDelayedAt = -1;

            if (!(candidate.When is null) && !this._receivedEvents.Contains(candidate.When))
                return;

            if (candidate.Pause)
                GameTime.I.Pause();
            else
                GameTime.I.Unpause();

            next.Enqueue(all.Dequeue());
            this._receivedEvents.Remove(candidate.When);
        }
    }
}