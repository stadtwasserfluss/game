#if UNITY_WEBGL
using Artefacts.Scripts.API.Yaml.WebGl.Sectors;
#else
using System.Linq;
using Artefacts.Scripts.API.Yaml;
#endif
using System;
using System.Collections.Generic;

namespace Artefacts.Scripts.API.Map {
    public sealed class SectorConfigurationList {
        private static Lazy<SectorConfigurationList> _lazyInstance = SectorConfigurationList.GetLazyInstance();

        private IReadOnlyDictionary<int, Sector> _availableSectorConfigurations;

        private SectorConfigurationList() {
            this.Initialize();
        }

        public static SectorConfigurationList Instance => SectorConfigurationList._lazyInstance.Value;

        public Sector GetById(int id) {
            if (!this._availableSectorConfigurations.ContainsKey(id))
                throw new ArgumentException($"Configuration file for sector {id} was not found.");

            return this._availableSectorConfigurations[id];
        }

        public static void Reset() {
            SectorConfigurationList._lazyInstance = SectorConfigurationList.GetLazyInstance();
        }

        private static Lazy<SectorConfigurationList> GetLazyInstance() {
            return new Lazy<SectorConfigurationList>(() => new SectorConfigurationList());
        }

        private void Initialize() {
#if UNITY_WEBGL
            this._availableSectorConfigurations = new Dictionary<int, Sector> {
                {1, new Sector01()},
                {2, new Sector02()},
                {3, new Sector03()},
                {4, new Sector04()},
                {5, new Sector05()},
                {6, new Sector06()},
                {7, new Sector07()},
                {8, new Sector08()},
                {9, new Sector09()},
                {10, new Sector10()},
                {11, new Sector11()},
                {12, new Sector12()},
                {13, new Sector13()},
                {14, new Sector14()},
                {15, new Sector15()}
            };
#else
            this._availableSectorConfigurations =
                (from f in FileLayer.GetYamlFilesInDirectory("data/sectors")
                 select YamlLoader<Sector>.Load(f)).ToDictionary(s => s.Id, s => s);
#endif
        }
    }
}