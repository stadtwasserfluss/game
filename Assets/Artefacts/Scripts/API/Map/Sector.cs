using System;
using System.Collections.Generic;
using System.Linq;
using Artefacts.Scripts.API.Gameplay;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CollectionNeverUpdated.Global
// ReSharper disable UnusedMember.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Artefacts.Scripts.API.Map {
    public class Sector : IGameTimeComponent {
        public enum ActionPlacementResult {
            Ok = 0, NotAllowedByLevel = 1, RequirementNotImplemented = 1 << 1, NotStackable = 1 << 2,
            BlueprintNotAllowed = 1 << 3, StackingDelayed = 1 << 4, StackLimitReached = 1 << 5, NoAreaLeft = 1 << 6,
            NotAllowedBySector = 1 << 7, NotEnoughMoney = 1 << 8, WrongGamePhase = 1 << 9
        }

        private readonly List<Action.Action> _placedActions;
        private int _availableGroundArea;
        private int _availableRoofArea;

        public Sector() {
            this._placedActions = new List<Action.Action>();
        }

        public int Id { get; protected set; }
        public float PublicAwarenessFactor { get; protected set; }
        public int TotalProtection => this._placedActions.Sum(a => a.GetBiasedProtection(this.RainIndex));

        public int WeightedProtection =>
            (int) Math.Round(this.TotalProtection *
                             GameManager.I.CurrentLevel.RainIndexDistribution.GetOrZero(this.Id));

        public IEnumerable<string> AvailableActions { get; protected set; }

        // ReSharper disable once CollectionNeverUpdated.Local
        public IDictionary<string, int> MaxStack { get; protected set; }
        public IEnumerable<Action.Action> PlacedActions => this._placedActions;
        public bool IsActive => GameManager.I.CurrentLevel?.ActiveSectors.Contains(this.Id) == true;
        public float RainIndex { get; set; }
        public float Importance { get; set; } = 1;
        public int AreaSector { get; protected set; }
        public int Water => (int) (this.AreaSector * this.RainIndex);
        public int Damage => (int) (this.Water * this.Importance);

        public int AvailableGroundArea {
            get => this._availableGroundArea - this._placedActions.Where(a => a.UsesGroundSpace).Sum(a => a.AreaUsage);
            protected set => this._availableGroundArea = value;
        }

        public int AvailableRoofArea {
            get => this._availableRoofArea - this._placedActions.Where(a => a.UsesRoofSpace).Sum(a => a.AreaUsage);
            protected set => this._availableRoofArea = value;
        }

        public void BeforeTimeStep(long now) { }

        public void OnTimeStep(long now) { }

        public void AfterTimeStep(long now) {
            this._placedActions.RemoveAll(a => !a.IsEffective);
        }

        public ActionPlacementResult PlaceAction(Action.Action action) {
            var apr = this.CheckActionPlacement(action);
            if (apr != ActionPlacementResult.Ok) return apr;

            this._placedActions.Add(action);
            return ActionPlacementResult.Ok;
        }

        public ActionPlacementResult CheckActionPlacement(Action.Action action, bool skipBluePrintCheck = false) {
            // The action is short term but the game is not in preparation phase or vice versa
            if (action.IsShortTerm && !EventManager.I.IsInPreparationPhase ||
                !action.IsShortTerm && EventManager.I.IsInPreparationPhase)
                return ActionPlacementResult.WrongGamePhase;

            // The action is not allowed by the level configuration.
            if (!GameManager.I.CurrentLevel.EnabledActions.Contains(action.Id))
                return ActionPlacementResult.NotAllowedByLevel;

            // The action is not allowed by the sector definition.
            if (!this.AvailableActions.Contains(action.Id))
                return ActionPlacementResult.NotAllowedBySector;

            // The player has not enough money
            if (!BankAccount.Instance.CanBuy(action, action.AreaUsage))
                return ActionPlacementResult.NotEnoughMoney;

            // The Sector has no more area left
            if (action.UsesGroundSpace && this.AvailableGroundArea - action.AreaUsage < 0 ||
                action.UsesRoofSpace && this.AvailableRoofArea - action.AreaUsage < 0)
                return ActionPlacementResult.NoAreaLeft;

            // The action has been already implemented and cannot be stacked.
            if (this.CountEffectiveImplementationsOfAction(action) == 1 && !action.IsStackable)
                return ActionPlacementResult.NotStackable;

            // The action can be stacked but the maximum stack value for this sector is exceeded.
            if (this.CountEffectiveImplementationsOfAction(action) >= this.GetMaxStackabilityForAction(action.Id))
                return ActionPlacementResult.StackLimitReached;

            // The action requires at least one other action to be implemented which is not implemented yet.
            if (action.RequiresImplementationOf.Except(this.PlacedActions.Where(a => a.IsEffective).Select(a => a.Id))
               .Any())
                return ActionPlacementResult.RequirementNotImplemented;

            // The last action was placed not long enough ago.
            if (this.PlacedActions.Where(a => a.Id == action.Id).Select(a => a.PlacedAt)
                   .DefaultIfEmpty(-action.StackDelay).Max() + action.StackDelay > GameTime.I.Now)
                return ActionPlacementResult.StackingDelayed;

            // The action is just a blueprint which is not timing-enabled.
            if (action.IsBlueprint && !skipBluePrintCheck)
                return ActionPlacementResult.BlueprintNotAllowed;

            return ActionPlacementResult.Ok;
        }

        public void RemoveAction(Action.Action action) {
            this.RemoveActions(action);
        }

        public void RemoveActions(params Action.Action[] actions) {
            this._placedActions.RemoveAll(a => actions.Select(b => b.Id).Distinct().Contains(a.Id));
        }

        private int CountEffectiveImplementationsOfAction(Action.Action action) {
            return this.CountEffectiveImplementationsOfAction(action.Id);
        }

        public int CountEffectiveImplementationsOfAction(string actionId) {
            return this.PlacedActions.Where(a => a.IsEffective).Count(a => a.Id == actionId);
        }

        public int GetMaxStackabilityForAction(string actionId) {
            return this.MaxStack.Any(kvp => kvp.Key == actionId)
                       ? this.MaxStack.First(kvp => kvp.Key == actionId).Value
                       : 1;
        }
    }
}