using System;
using System.Collections.Generic;
using System.Linq;
using Artefacts.Scripts.API.Gameplay;
using JetBrains.Annotations;

namespace Artefacts.Scripts.API.Map {
    public class SectorMap : IGameTimeComponent {
        public SectorMap([NotNull] IEnumerable<Sector> sectors) {
            this.AllSectors = sectors ?? throw new ArgumentNullException(nameof(sectors));

            GameTime.I.AddComponent(this);
        }

        public IEnumerable<Sector> AllSectors { get; }
        public int TotalWeightedProtection => this.AllSectors.Sum(s => s.WeightedProtection);
        public int TotalProtection => this.AllSectors.Sum(s => s.TotalProtection);
        public int TotalArea => this.AllSectors.Sum(s => s.AreaSector);
        public int TotalWater => this.AllSectors.Sum(s => s.Water);
        public int TotalDamage => this.AllSectors.Sum(s => s.Damage);

        public void BeforeTimeStep(long now) {
            var em = EventManager.I;
            var level = GameManager.I.CurrentLevel;

            if (!em.IsInPreparationPhase) return;

            var rain = em.GetCurrentCatastropheNotification()?.Rain;
            if (rain is null) return;

            foreach (var sector in rain.AffectedObjects)
                sector.RainIndex = rain.Index * level.RainIndexDistribution.GetOrZero(sector.Id);

            foreach (var sector in this.AllSectors) sector.Importance = level.SectorImportance.GetOrOne(sector.Id);
        }

        public void OnTimeStep(long now) { }

        public void AfterTimeStep(long now) { }

        public Sector GetSectorById(int id) {
            return this.AllSectors.FirstOrDefault(s => s.Id == id);
        }
    }
}