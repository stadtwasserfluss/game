#if UNITY_WEBGL
using Artefacts.Scripts.API.Yaml.WebGl.Actions;
#else
using Artefacts.Scripts.API.Yaml;
#endif
using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Artefacts.Scripts.API.Action {
    public sealed class ActionList {
        private static Lazy<ActionList> _lazyInstance = ActionList.GetLazyInstance();
        private Dictionary<string, Action> _availableProtections;

        private ActionList() {
            this.Initialize();
        }

        public static ActionList I => ActionList._lazyInstance.Value;

        [CanBeNull]
        public Action GetById(string id) {
            if (this.IsAvailable(id)) return this._availableProtections[id].GetCopy();

            Debug.LogError($"Action with the id '{id}' is not available");
            return null;
        }

        [CanBeNull]
        public Action GetBlueprintById(string id) {
            if (this.IsAvailable(id)) return this._availableProtections[id].GetBlueprintCopy();

            Debug.LogError($"Action with the id '{id}' is not available");
            return null;
        }

        public bool IsAvailable(string id) {
            return this._availableProtections.ContainsKey(id);
        }

        public static void Reset() {
            ActionList._lazyInstance = ActionList.GetLazyInstance();
        }

        private static Lazy<ActionList> GetLazyInstance() {
            return new Lazy<ActionList>(() => new ActionList());
        }

        private void Initialize() {
#if UNITY_WEBGL
            this._availableProtections = new Dictionary<string, Action> {
                {"cs", new ActionClosingStreets()},
                {"g", new ActionGreenRoof()},
                {"m", new ActionMultipurposeArea()},
                {"sb", new ActionSandbags()},
                {"s", new ActionSewerSystem()},
                {"p", new ActionStormwaterPlanters()},
                {"pg", new ActionUndergroundGarage()},
                {"u", new ActionUnsealing()}
            };
#else
            this._availableProtections = new Dictionary<string, Action>();
            foreach (string fileName in FileLayer.GetYamlFilesInDirectory("data/actions")) {
                var p = YamlLoader<Action>.Load(fileName);
                this._availableProtections.Add(p.Id, p);
            }
#endif
        }
    }
}