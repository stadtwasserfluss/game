using System.Collections.Generic;
using Artefacts.Scripts.API.Gameplay;

// ReSharper disable UnusedMember.Local
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Artefacts.Scripts.API.Action {
    public class Action : IGameTimeComponent, IBuyable {
        private const int UNLIMITED_LIFETIME = -1;
        private int _floodEffect;
        private int _rainEffect;

        public string Id { get; protected set; }
        public string HumanReadableName { get; protected set; }
        public int BuildTime { get; protected set; }
        public bool UsesGroundSpace { get; protected set; }
        public bool UsesRoofSpace { get; protected set; }
        public int AreaUsage { get; protected set; }
        public bool SurvivesQuest { get; protected set; }
        public bool IsStackable { get; protected set; }
        public int StackDelay { get; protected set; }
        public int LifeTime { get; protected set; }
        public bool InfluencedByPublicAwareness { get; protected set; }
        public IEnumerable<string> RequiresImplementationOf { get; protected set; }
        public int BuildTimeLeft { get; protected set; }
        public int LifeTimeLeft { get; protected set; }
        public bool IsConstructed => this.BuildTimeLeft == 0;
        public bool IsBlueprint { get; protected set; } = true;
        public long PlacedAt { get; protected set; }
        public bool IsShortTerm { get; protected set; }

        // ReSharper disable once CollectionNeverUpdated.Global
        public IDictionary<int, float> EffectivenessPerIndex { get; protected set; }
        public int Protection => this.RainEffect * this.AreaUsage;

        public bool IsEffective =>
            this.IsConstructed && (this.LifeTimeLeft > 0 || this.LifeTime == Action.UNLIMITED_LIFETIME);

        public int RainEffect {
            get => this.IsEffective ? this._rainEffect : 0;
            protected set => this._rainEffect = value;
        }

        public int Cost { get; protected set; }

        public void BeforeTimeStep(long now) {
            if (!this.IsShortTerm && EventManager.I.IsInPreparationPhase)
                return;

            if (!this.IsConstructed)
                this.BuildTimeLeft--;
            else if (this.IsEffective)
                this.LifeTimeLeft--;
        }

        public void OnTimeStep(long now) { }

        public void AfterTimeStep(long now) { }

        public int GetBiasedProtection(float rainIndex = 1) {
            return (int) (this.Protection * this.EffectivenessPerIndex.GetOrDefault((int) rainIndex, 1));
        }

        public Action GetCopy() {
            var copy = this.GetBlueprintCopy();

            copy.IsBlueprint = false;
            copy.InitTiming();

            return copy;
        }

        public Action GetBlueprintCopy() {
            return (Action) this.MemberwiseClone();
        }

        public override string ToString() {
            return this.HumanReadableName;
        }

        private void InitTiming() {
            var gt = GameTime.I;
            gt.AddComponent(this);
            this.BuildTimeLeft = this.BuildTime;
            this.LifeTimeLeft = this.LifeTime;
            this.PlacedAt = gt.Now;
        }
    }
}