using System;
using System.IO;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Artefacts.Scripts.API.Yaml {
    public static class YamlLoader<T> {
        public static T Load(string relative) {
            string extension = Path.GetExtension(relative);

            if (extension != ".yaml")
                throw new ArgumentException("Only files with the .yaml extension can be loaded.", nameof(relative));

#if UNITY_ANDROID && !UNITY_EDITOR
            if (!BetterStreamingAssets.FileExists(relative))
                throw new FileNotFoundException($"The configuration file ({relative}) could not be found.", relative);
            
            string input = BetterStreamingAssets.ReadAllText(relative);
#else
            string absolute = Path.Combine(FileLayer.GetRootDirectory(), relative);

            if (!File.Exists(absolute))
                throw new FileNotFoundException(
                    $"The configuration file ({relative}, on disk: {absolute}) could not be found.", absolute);

            string input = File.ReadAllText(absolute);
#endif

            return new DeserializerBuilder()
               .WithNamingConvention(new PascalCaseNamingConvention())
               .Build()
               .Deserialize<T>(input);
        }
    }
}