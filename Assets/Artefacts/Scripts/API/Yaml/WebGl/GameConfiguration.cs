#if UNITY_WEBGL
using Artefacts.Scripts.API.Gameplay;

namespace Artefacts.Scripts.API.Yaml.WebGl {
    public class GameConfiguration : Configuration {
        public GameConfiguration() {
            this.GameStepDuration = 1000;
            this.StartGamePaused = true;
            this.PauseAtNewLevel = false;
        }
    }
}
#endif