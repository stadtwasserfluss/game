#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Communication;
using Artefacts.Scripts.API.Communication.Quiz;

namespace Artefacts.Scripts.API.Yaml.WebGl.Story {
    public class Level02Story : Communication.Story {
        public Level02Story() {
            this.Level = 2;
            this.Messages = new List<Message> {
                new Message {
                    When = "closed-email-1",
                    Delay = 0,
                    From = "Carl",
                    Text = @"Da hast du aber viel Lob kassiert.
",
                },

                new Message {
                    When = null,
                    Delay = 3,
                    From = "Carl",
                    Text = @"Dafür, dass ich die ganze Arbeit getan habe.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text = @"Gewöhn dich nicht dran. Von jetzt an, bist du auf dich allein gestellt.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text =
                        @"Unten links auf der Karte siehst du, welche Sektoren dir aktuell zugeteilt sind. Im Fall eines Starkregenereignisses siehst du auch, welche Sektoren wahrscheinlich betroffen sein werden.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text =
                        @"Du kannst dich jetzt mit W,A,S,D oder mit den Pfeiltasten bewegen. Alternativ kannst du auch auf die Minimap klicken und gelangst direkt zu dem entsprechenden Sektor.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text =
                        @"Kleiner Tipp: In manchen Sektoren richten Überflutungen durch Starkregenereignisses mehr Schaden an, als in anderen.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 5,
                    From = "Carl",
                    Text = @"... solche mit nem Dom zum Beipiel.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text =
                        @"Du solltest außerdem bedenken, dass Starkregen ein unplanbares Ereignis ist. Wettervorhersagen treffen nicht immer zwangsläufig zu.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text =
                        @"Starkregen kann sich in anderen Bereichen ereignen, das Wasser kann entlang von Fließwegen an der Geländeoberfläche in andere Sektoren gelangen oder durch ein überfülltes Abwassersystem an anderer Stelle austreten.    
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text =
                        @"Auch deine Maßnahmen können ab bestimmten Regenmengen eine verminderte Wirkung haben oder sogar versagen.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 6,
                    From = "Carl",
                    Text =
                        @"Deswegen solltest du nicht vergessen die Öffentlichkeit zu informieren und dir damit ihre Koorperation zu sichern.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 5,
                    From = "Carl",
                    Text =
                        @"Wenn du sie nicht informierst, verblasst mit der Zeit ihre Erinnerung und Sorge bezüglich Starkregen.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text =
                        @"Außerdem gilt: Je stärker ein Starkregenereignis, desto wichtiger ist die Rolle der privaten Maßnahmen.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 6,
                    From = "Carl",
                    Text =
                        @"Deine Aufgabe ist es jetzt also mit den Mitteln, die Dir hier zur Verfügung stehen, Maßnahmen zu implementieren und die Stadt vor zukünftigen Starkregenereignissen zu schützen. Versuche hierbei Deine Schritte zu planen und schaue, welche Maßnahmen, in welchem Sektor umsetzbar sind und welche schon gebaut wurden.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 7,
                    From = "Carl",
                    Text = @"(Klicke auf Play um fortzufahren.)
",
                    Pause = true,
                },
            };
            this.Emails = new List<Email> {
                new Email {
                    Id = 1,
                    When = null,
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " LEVEL 2",
                    Text = @"Sie haben mich nicht enttäuscht!
nachdem Sie sich beim letzten Starkregenereignis so unter Beweis gestellt haben, konnte ich den restlichen Stadtrat davon überzeugen Ihnen mehr Sektoren der Stadt zur Verfügung zu stellen und Ihnen mehr Planungsfreiheiten einzuräumen. Kommunales Starkregenrisikomanagement umfasst viele Akteure und einen guten Mix an verschiedenen Maßnahmen. Sie müssen sich nun gut überlegen, welche Maßnahmen bereits implementiert sind und welche Sie sich leisten können, um eine gute Mischung zu finden. Es wird jetzt schwerer dies zu koordinieren und sich um alles zu kümmern.
Genießen Sie die neugewonnene Freiheit, aber vergessen Sie nicht: Starkregenrisikomanagement ist Teamarbeit! 

Mit freundlichen Grüßen
--
Dr.- Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
                new Email {
                    Id = 2,
                    When = "preparation-start",
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " UNWETTERWARNUNG",
                    Text = @"Liebes StadtWasserFluss Team,
wir wurden auf einen heranziehendes Unwetter aufmerksam gemacht. Erwartet wird ein Starkregenereignis eines SRI von 6. Bereiten Sie alle entsprechenden Maßnahmen vor.

Gutes Gelingen!

Mit freundlichen Grüßen
--
Dr.- Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
                new Email {
                    Id = 3,
                    When = "at-201",
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " Informationen zur Pressemitteilung",
                    Text = @"Je stärker ein Starkregenereignis, desto wichtiger ist die Rolle der privaten Maßnahmen.
Einmal implementiert schützt eine Maßnahme nicht vor jedem Starkregenereignis.
Durch eine gute Kommunikation mit der Öffentlichkeit kann eine höhere Akzeptanz von öffentlichen Maßnahmen und eine Einsatzbereitschaft von Privatpersonen, Maßnahmen umzusetzen, erreicht werden.
Die Kommunikation mit der Öffentlichkeit sollte wiederkehrend initiiert werden, um die Erinnerung und die Sorge bezüglich Starkregenereignissen aufrecht zu erhalten. Aber übertreiben Sie es nicht!

Mit freundlichen Grüßen
--
Dr.- Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
                new Email {
                    Id = 4,
                    When = "at-223",
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " Nochmal gut ausgegangen!",
                    Text = @"Liebes StadtWasserFluss Team,
wieder ein großes Lob an Sie. Der Verlauf von Starkregen ist leider nicht vorhersehbar und dieses Ereignis war keine Ausnahme. Unser Abwassersystem in StadtWasserFluss ist nur auf ein etwa 30jährliches Ereignis ausgelegt (~ SRI 5). Durch Maßnahmen der Retention aber auch zum Beispiel durch Flächenentsiegelung konnten Sie erreichen, dass Wasser zurückgehalten wird und so auch weniger Wasser in das Kanalnetz abgegeben wird. Durch Ihre schnelle Reaktion, die Einbindung der Öffentlichkeit und Ihre vorausschauende Planung konnte ein Großteil des Schadens verhindert werden.

Weiter so!

Mit freundlichen Grüßen
--
Dr.- Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
            };
            this.Questions = new List<Question> {
                new Question {
                    Id = 1,
                    When = "at-200",
                    Text = @"Vervollständigen Sie: Je stärker ein Starkregenereignis ist, desto wichtiger ist...",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text = @"die Rolle des Abwassersystems.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text = @"die Rolle der privaten Maßnahmen.",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text = @"die Rolle der Gründächer.",
                            IsCorrect = false
                        },
                    },
                },
                new Question {
                    Id = 2,
                    When = null,
                    Text = @"Einmal implementiert, schützt eine Maßnahme vor jedem Starkregenereignis.",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text = @"Richtig",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text = @"Falsch",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text = @"Das kommt auf die Maßnahme an.",
                            IsCorrect = false
                        },
                    },
                },
                new Question {
                    Id = 3,
                    When = null,
                    Text =
                        @"Was kann neben einer Einsatzbereitschaft von Privatpersonen Maßnahmen umzusetzen noch durch gute Kommunikation mit der Öffentlichkeit erreicht werden?",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text = @"Eine höhere Akzeptanz von öffentlichen Maßnahmen.",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text = @"Dass kein Schaden durch Starkregen entsteht.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text = @"Generierung eines größeren Budget durch mehr Einnahmen.",
                            IsCorrect = false
                        },
                    },
                },
                new Question {
                    Id = 4,
                    When = null,
                    Text = @"Sollte Kommunikation und damit öffentliches Bewusstsein wiederkehrend initiiert werden?",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text =
                                @"Nein, bei einem guten Kommunikationskonzept ist dies nicht notwendig, da die Unterlagen jederzeit auf verschiedenen Kanälen im Internet abgerufen werden können.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text =
                                @"Ja, denn die Erinnerungen (an Starkregenereignisse oder ihre Möglichkeit) werden mit der Zeit schwächer, ebenso wie die Sorge und der Fleiß, private Schutzmaßnahmen zu ergreifen. Dabei muss aktiv kommuniziert werden.",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text =
                                @"Nur, wenn es von Seiten der allgemeinen Bevölkerung erwünscht ist und sich diese z.B. in Mailinglisten eingetragen haben.",
                            IsCorrect = false
                        },
                    },
                },
            };
        }
    }
}
#endif