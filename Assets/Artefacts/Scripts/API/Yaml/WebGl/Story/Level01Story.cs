#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Communication;
using Artefacts.Scripts.API.Communication.Quiz;

namespace Artefacts.Scripts.API.Yaml.WebGl.Story {
    public class Level01Story : Communication.Story {
        public Level01Story() {
            this.Level = 1;
            this.Messages = new List<Message> {
                new Message {
                    When = "closed-email-1",
                    Delay = 0,
                    From = "Carl",
                    Text = @"Oh, wow. Du bist es wirklich.
",
                },

                new Message {
                    When = null,
                    Delay = 2,
                    From = "Carl",
                    Text = @"Ich weiß wirklich nicht, was Dr. Capo in dir sieht...
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text = @"Wie auch immer.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text =
                        @"Das hier ist StadtWasserFluss, unser Echtzeit-Starkregen-Anpassungssytem! Hiermit kannst du alle kommunalen Maßnahmen Aachens steuern.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 6,
                    From = "Carl",
                    Text = @"Naja, erst mal nur Sektor 10. Zum Üben.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 5,
                    From = "Carl",
                    Text = @"Klick mal auf die Stadt selbst. Damit öffnest du die Oberfläche von StadtWasserFluss.
",
                    Pause = true,
                },

                new Message {
                    When = "selected-sector-10",
                    Delay = 0,
                    From = "Carl",
                    Text =
                        @"Wie schon gesagt: Vor dir siehst du Sektor 10 von Aachen. Auf der Karte unten links siehst du, wo sich der Sektor in etwa befindet.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text =
                        @"Da drüber siehst du alle Maßnahmen, die in diesem Sektor angewandt werden können. Und natürlich, welche bereits implementiert wurden.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 7,
                    From = "Carl",
                    Text =
                        @"Außerdem siehst du, wie viel Dach- und Bodenfläche dir zur Verfügung stehen. Diese Werte solltest du im Auge behalten.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 6,
                    From = "Carl",
                    Text = @"Wie wäre es damit, dass wir zunächst ein paar Gründächer implementieren?
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 6,
                    From = "Carl",
                    Text =
                        @"Rechts siehst du alle Maßnahmen. Wenn du mit der Maus darüber fährst, bekommst du zusätzliche Informationen.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text = @"Wähle einfach die Gründächer aus indem du auf den Button drückst.
",
                    Pause = true,
                },

                new Message {
                    When = "button-action-deploy-g",
                    Delay = 0,
                    From = "Carl",
                    Text = @"Das dauert jetzt etwas.
",
                },

                new Message {
                    When = null,
                    Delay = 5,
                    From = "Carl",
                    Text = @"Sag mal, kann es sein, dass du dich gar nicht an mich erinnerst?
",
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text = @"Carl? Von der Uni??
",
                },

                new Message {
                    When = null,
                    Delay = 5,
                    From = "Carl",
                    Text = @"Ja, genau! Der Starkregenexperte!
",
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text = @"Aber natürlich geben sie DIR den Job. Ich bin mir sicher, das ist totaaaal gerechtfertigt.
",
                },

                new Message {
                    When = null,
                    Delay = 6,
                    From = "Carl",
                    Text = @"Tja BOSS, erinnerst du dich überhaupt daran, wie man Starkregen definiert?
",
                },

                new Message {
                    When = null,
                    Delay = 5,
                    From = "Carl",
                    Text = @"Starkregen ist:
",
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text =
                        @"ein Regenereignis, das im Verhältnis zu seiner Dauer eine hohe Niederschlagsintensität besitzt. Das heißt: Viel Regen in kurzer Zeit.
",
                },

                new Message {
                    When = null,
                    Delay = 7,
                    From = "Carl",
                    Text = @"ODER
",
                },

                new Message {
                    When = null,
                    Delay = 3,
                    From = "Carl",
                    Text = @"eine durchschnittliche Niederschlagsmenge über einen langen Zeitraum hinweg.
",
                },

                new Message {
                    When = null,
                    Delay = 6,
                    From = "Carl",
                    Text = @"Kommunalen Maßnahmen alleine reichen bei heftigen Ereignissen nicht aus.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 6,
                    From = "Carl",
                    Text =
                        @"Nur in Zusammenarbeit mit privaten Grundstückbesitzerinnen und -besitzern können wir die Stadt wirklich schützen.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 7,
                    From = "Carl",
                    Text =
                        @"Ein wesentlicher Teil der Verantwortung liegt nämlich bei ihnen, daher ist es wichtig, die Öffentlichkeit zu informieren.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 7,
                    From = "Carl",
                    Text =
                        @"Unten, neben deiner Budget- und Tagesanzeige findest du das Aufmerksamkeitslevel der Öffentlichkeit.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 6,
                    From = "Carl",
                    Text =
                        @"Wenn du da drauf klickst, kannst du sie für die Thematik sensibilisieren und damit erreichen, dass sie entweder selbst Maßnahmen umsetzen oder die Akzeptanz für deine kommunalen Maßnahmen erhöht wird.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text =
                        @"Mit der Zeit vergessen sie das Thema allerdings langsam wieder. Wenn du es aber übertreibst, nervst du sie nur. Also versuch die goldene Mitte zu finden.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text = @"(Drücke auf das Play-Zeichen um fortzufahren.)
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text = @"Du kannst übrigens auch die Zeit beschleunigen, indem du oben links auf das Menü klickst.
",
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text = @"Ich weiß, ich bin genial. Kein Grund mir zu danken.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 6,
                    From = "Carl",
                    Text = @"Natürlich kannst du zwischendurch auch bei mir abgucken. Genau wie damals in der Uni.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 6,
                    From = "Carl",
                    Text =
                        @"Zum Beispiel, indem du auf meine tollen Inforgrafiken guckst. Klicke dazu auf den Briefumschlag oben rechts. 
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 3,
                    From = "Carl",
                    Text = @"(Drücke auf das Play-Zeichen um fortzufahren.)
",
                    Pause = true,
                },

                new Message {
                    When = "closed-email-2",
                    Delay = 0,
                    From = "Carl",
                    Text = @"Wenn du mal nicht ein Glückpilz bist! Direkt ein Starkregenereignis!
",
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text =
                        @"Sobald wir eine Unwetterwarnung erhalten sind wir befugt kurzfristige Maßnahmen zu nutzen, die nach dem Starkregenereignis wieder abgebaut werden. 
",
                },

                new Message {
                    When = null,
                    Delay = 7,
                    From = "Carl",
                    Text = @"Du findest sie auf der rechten Seite unter den regulären Maßnahmen.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 5,
                    From = "Carl",
                    Text = @"Versuch z.B. mal Sandsäcke zu bauen.
",
                    Pause = true,
                },

                new Message {
                    When = "button-action-deploy-sb",
                    Delay = 0,
                    From = "Carl",
                    Text = @"Frau Capo hat den SRI erwähnt. 
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text = @"Weißt du, was das heißt?
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text = @"Es steht für STARKREGENINDEX.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text =
                        @"Der Starkregenindex beschreibt das Ausmaß eines Events auf einer Skala von 1 bis 12. Daraus ergeben sich verschiedene Stufen in Abhängigkeit von der Häufigkeit.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 10,
                    From = "Carl",
                    Text = @"Verstanden?
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text =
                        @"Gut. Denn du bist dafür veranwortlich, eine Pressemitteilung zu schreiben. Dazu haben wir eine praktische Vorlage entwickelt. Die siehst du gleich.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text = @"Schreib erst mal über grundlegende Infos zu Starkregen.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 5,
                    From = "Carl",
                    Text = @"Viel Spaß! 
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 3,
                    From = "Carl",
                    Text = @"(Drücke auf das Play-Zeichen neben der Tages-/Stundenanzeige)
",
                    Pause = true,
                },
            };
            this.Emails = new List<Email> {
                new Email {
                    Id = 1,
                    When = null,
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " LEVEL 1",
                    Text = @"Willkommen im Team!
Wir freuen uns sehr, Sie als neues Mitglied im Amt für Starkregen und Sturzfluten der Stadt Aachen begrüßen zu dürfen.
Natürlich begleiten wir Sie auf dem Weg, Aachen an den Klimawandel anzupassen.
Uns ist bewusst, dass dies Ihre erste Managementposition ist und Sie bislang auch nicht im Bereich Klimawandelanpassung gearbeitet haben. Dennoch sehe ich großes Potential in Ihren Kommunikations– und Organisationsskills. Mit ihren herausragenden Planungsfähigkeiten gelingt es Ihnen ggf. sogar das Etat der Abteilung zu vergrößern.
Zunächst wird Ihnen nur Sektor 10 der Stadt zugeteilt und Ihnen stehen nicht alle Maßnahmen zur Verfügung. Wenn Sie sich hier beweisen, sehe ich kein Problem Ihre Verantwortung in der nahen Zukunft zu erweitern. Jetzt am Anfang möchte ich jedoch kein Risiko eingehen.
Zur Orientierung wird sich bald Ihr persönlicher Assistent melden und Ihnen unser Arbeitstool vorstellen. Außerdem finden Sie hilfreiche Tipps in unseren Infografiken.

Gutes Gelingen!

Mit freundlichen Grüßen
--
Dr.- Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
                new Email {
                    Id = 2,
                    When = "preparation-start",
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " UNWETTERWARNUNG",
                    Text = @"Liebes StadtWasserFluss Team,
wir wurden auf einen heranziehendes Unwetter aufmerksam gemacht. Erwartet wird ein Starkregenereignis eines SRI von 2. Bereiten Sie alle entsprechenden Maßnahmen vor.

Gutes Gelingen!

Mit freundlichen Grüßen
--
Dr.- Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
                new Email {
                    Id = 3,
                    When = "at-93",
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " Informationen zur Pressemitteilung",
                    Text = @"Liebes StadtWasserFluss Team,
ein Starkregen ist ein Regenereignis von kurzer Dauer mit hoher Niederschlagsmenge, die kleinräumig und räumlich scharf abgegrenzt sind. Der genaue Niederschlagsort kann oft nicht vorhergesagt werden.
Starkregenereignisse können klassifiziert werden. Zum Beispiel durch den Starkregenindex (SRI), der Ereignisse auf Basis ihrer Intensität auf einer Skala von 1 bis 12 einordnet.
Diese SRIs entsprechen Starkregen (SRI 1-2), intensivem Starkregen (SRI 3-5), starkem Starkregen (SRI 6-7), and extremen Starkregen (SRI 8-12).
Starkregenmanagement ist nicht ausschließlich die Aufgabe der Stadt- bzw. Kommunalverwaltung. Auch die Bevölkerung trägt Verantwortung und um diese mit einzubeziehen, ist es eine wichtige Aufgabe der Kommunen, die Bevölkerung und lokale Firmen über die Gefahren und Risiken von Starkregen und über mögliche Maßnahmen zu informieren.

Mit freundlichen Grüßen
--
Dr.- Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
                new Email {
                    Id = 4,
                    When = "at-94",
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " Gute Arbeit!",
                    Text = @"Liebes StadtWasserFluss Team,
ein großes Lob daran, wie Sie alle diese Situation gemeistert haben. Solche Ergebnisse zeigen, dass wir etwas bewegen können.
Sie haben auf das Ausmaß des Regenereignisses geachtet und Ihre Maßnahmenauswahl darauf ausgerichtet. Gründächer halten zwar nur eine vergleichsweise geringe Regenmenge pro Quadratmeter zurück, doch tragen sie allgemein zum Stadtklima bei. So sind solche Maßnahmen trotzdem interessant für uns.
Auch der gezielte Einsatz von kurzfristigen Maßnahmen wie Sandsäcke können dabei helfen das Eintreten von Wasser durch Türen oder Fenster zu verhindern. Aber nicht immer ist die Vorwarnzeit lang genug, dass ausreichend Sandsäcke zur Verfügung stehen. Diese Maßnahme ist daher nur für kleine Bereiche geeignet.

Weiter so!

Mit freundlichen Grüßen
--
Dr.- Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
            };
            this.Questions = new List<Question> {
                new Question {
                    Id = 1,
                    When = "at-92",
                    Text = @"Starkregen ist...",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text = @"ein Regenereignis von kurzer Dauer mit niedriger Niederschlagsmenge.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text = @"ein Regenereignis von kurzer Dauer mit hoher Niederschlagsmenge.",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text = @"ein Regenereignis von langer Dauer mit niedriger Niederschlagsmenge.",
                            IsCorrect = false
                        },
                    },
                },
                new Question {
                    Id = 2,
                    When = null,
                    Text = @"Welche der nachfolgenden Aussagen hinsichtlich Starkregen stimmt? Starkregen sind..",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text =
                                @"kurze Niederschlagsereignisse mit hoher Regenmenge, großflächig und räumlich scharf abgegrenzt. Der genaue Niederschlagsort kann oft vorhergesagt werden.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text =
                                @"kurze Niederschlagsereignisse mit hoher Regenmenge, kleinräumig und räumlich nicht klar definierbar. Der genaue Niederschlagsort kann oft nicht vorhergesagt werden.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text =
                                @"kurze Niederschlagsereignisse mit hoher Regenmenge, kleinräumig und räumlich scharf abgegrenzt. Der genaue Niederschlagsort kann oft nicht vorhergesagt werden.",
                            IsCorrect = true
                        },
                    },
                },
                new Question {
                    Id = 3,
                    When = null,
                    Text = @"Starkregenereignisse können klassifiziert werden durch...",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text =
                                @"den Starkregenindex (SRI), der Ereignisse auf Basis ihrer Intensität auf einer Skala von 1 bis 12 einordnet.",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text =
                                @"den Starkregenkatalog (SRK), welcher verschiedene Starkregenereignisse und ihre möglichen Folgen kategorisiert. ",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text =
                                @"die Starkregenrate (SRR), welche beschreibt, wie viel Regen bei einem Ereignis fallen wird.",
                            IsCorrect = false
                        },
                    },
                },
                new Question {
                    Id = 4,
                    When = null,
                    Text = @"Diese SRIs entsprechen....",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text =
                                @"den verschiedenen Stadien des Niederschlags: dem Anfangsstadium, dem Höhepunkt und dem zurückweichenden Stadium.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text =
                                @"Starkregen (SRI 1-2), intensivem Starkregen (SRI 3-5), starkem Starkregen (SRI 6-7), and extremen Starkregen (SRI 8-12).",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text =
                                @"mildem Regen (SRI 1-3), mittelstarkem Regen (SRI 4-8), and sehr starkem Regen = Starkregen (SRI 9-12).",
                            IsCorrect = false
                        },
                    },
                },
                new Question {
                    Id = 5,
                    When = null,
                    Text = @"Starkregenmanagement ist ausschließlich die Aufgabe der Stadt- bzw. Kommunalverwaltung.",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text = @"Ja, dem stimme ich zu.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text = @"Nein, dem stimme ich nicht zu.",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text = @"Das kommt ganz auf die betrachtete Kommune an.",
                            IsCorrect = false
                        },
                    },
                },
                new Question {
                    Id = 6,
                    When = null,
                    Text =
                        @"Nun, da Sie wissen, dass die Vermeidung oder Verringerung von Schäden durch Starkregenereignisse in der Verantwortung der Kommune und aller Beteiligten liegt, was sollten Sie tun, um die Bevölkerung einzubeziehen?",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text = @"Nichts, es ist ihre Aufgabe, ihr Heim zu sichern.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text =
                                @"Um das Bewusstsein der Bevölkerung zu schärfen, ist es Ihre Aufgabe, sie einzuladen, um die bisher geleistete Arbeit zu begutachten.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text =
                                @"Eine wichtige Aufgabe der Kommunen ist es, die Bevölkerung und lokale Firmen über die Gefahren und Risiken von Starkregen und über mögliche Maßnahmen zu informieren.",
                            IsCorrect = true
                        },
                    },
                },
            };
        }
    }
}
#endif