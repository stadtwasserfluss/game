#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Communication;
using Artefacts.Scripts.API.Communication.Quiz;

namespace Artefacts.Scripts.API.Yaml.WebGl.Story {
    public class Level03Story : Communication.Story {
        public Level03Story() {
            this.Level = 3;
            this.Messages = new List<Message> {
                new Message {
                    When = "closed-email-1",
                    Delay = 0,
                    From = "Carl",
                    Text = @"Okay Okay, ich gebe zu, diesmal hast du gute Arbeit geleistet. 
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 5,
                    From = "Carl",
                    Text = @"Aber du musst auch zugeben, dass meine Tipps super hilfreich sind. Oder??
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 5,
                    From = "Carl",
                    Text = @"Der Stadtrat hat dir diesmal alle Sektoren überlassen. Das ist eine Menge Verantwortung...
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text =
                        @"Du solltest vor allem nicht vergessen, dass nicht nur der Regen selbst eine Gefahr darstellen kann. Das Risiko bestimmt sich auch durch die jeweilige Anfälligkeit zu Schäden oder durch den Wert eines Gebiets.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text = @"Wenn du weißt, welche Gebiete besonders anfällig sind, kannst du dein Handeln priorisieren.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text =
                        @"Denk außerdem daran, dass sich das Wasser auf dem Landweg, besonders auf versiegelten Oberflächen, dem topografisch tiefsten Punkt zubewegt. Auch kann bei Überlastung Wasser aus dem Kanalnetz austreten.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text =
                        @"Baulich, technische Maßnahmen allein reichen nicht aus, um eine Kommune vor Starkregen zu schützen.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text =
                        @"In manchen Gebieten sind diese nicht hinreichend umsetzbar. Hier musst du verstärkt mit temporären Maßnahmen oder Verhaltens- und Nutzungsänderungen arbeiten.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 8,
                    From = "Carl",
                    Text = @"Aber das wusstest du wahrscheinlich schon.
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 4,
                    From = "Carl",
                    Text = @"Ich überlasse dir das Steuer, BOSS! 
",
                    Pause = true,
                },

                new Message {
                    When = null,
                    Delay = 3,
                    From = "Carl",
                    Text = @"(Klicke auf Play um fortzufahren.)
",
                    Pause = true,
                },
            };
            this.Emails = new List<Email> {
                new Email {
                    Id = 1,
                    When = null,
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " LEVEL 3",
                    Text = @"Liebes StadtWasserFluss Team,
nach Ihren einschlägigen Erfolgen in der Vergangenheit haben wir uns entschieden, Ihnen nun die Verantwortung über alle Sektoren Aachens zu überlassen. Sie müssen also nicht mehr nur in den einzelnen Sektoren arbeiten, sondern darüber hinaus das Gesamtkonzept der Stadt entwickeln und im Blick behalten. Aber ich bin mir sicher, das schaffen Sie mit links!
Auch wenn Sie bereits große Teile der Stadt angepasst haben, so vermuten wir, dass die Frequenz und Intensität von Starkregenereignissen in der Zukunft, aufgrund des Klimawandels, nur zunehmen werden.  
Ihre Kommunikationfähigkeiten haben Sie bereits unter Beweis gestellt. Sie sollten dies nun vermehrt tun. Vergessen Sie nicht, dass es sich beim Starkregenrisikomanagement um ein interdisziplinäres Feld handelt, bei dem viele Akteure und Entscheider zusammenkommen.
Berücksichtigen Sie dies bei Ihrer Planung.

Mit freundlichen Grüßen
--
Dr.-Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
                new Email {
                    Id = 2,
                    When = "preparation-start",
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " UNWETTERWARNUNG",
                    Text = @"Liebes StadtWasserFluss Team,
wir wurden auf einen heranziehendes Unwetter aufmerksam gemacht. Erwartet wird ein Starkregenereignis eines SRI von 8. Bereiten Sie alle entsprechenden Maßnahmen vor.

Gutes Gelingen!

Mit freundlichen Grüßen
--
Dr.-Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
                new Email {
                    Id = 3,
                    When = "at-378",
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " Informationen zur Pressemitteilung",
                    Text =
                        @"Risikomanagement muss als kontinuierlicher, dynamischer Prozess verstanden werden, denn Starkregenereignisse werden aufgrund des Klimawandels voraussichtlich an Häufigkeit und Intensität zunehmen.
Wasser bewegt sich auf dem Landweg, besonders auf versiegelten Oberflächen, Richtung topografisch tiefsten Punkt. Auch kann bei einer Überlastung des Entwässerungsnetz Wasser austreten.  
Wenn Sie das das Risiko eines Gebiets kennen, können Sie Maßnahmen priosieren und Ihre Strategie anpassen.
Baulich-technische Maßnahmen alleine reichen nicht aus, denn in manchen Gebieten sind diese Maßnahmen nicht hinreichend umsetzbar. Hier muss verstärkt mit temporären Maßnahmen oder Verhaltens- und Nutzungsänderungen gearbeitet werden.
Zwar bietet die Entwicklung eines Starregenrisikomanagements auf Quartiersebene einen Ansatz, einzelne Maßnahmenkonzepte zu bündeln und aufeinander abzustimmen, dennoch umfasst ein funktionierendes Konzept die gesamte Kommune.

Mit freundlichen Grüßen 
--
Dr.-Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
                new Email {
                    Id = 4,
                    When = "at-400",
                    Delay = 0,
                    From = "Dr.- Ing. Ella Capo",
                    FromAdress = "capo@swf.com",
                    Subject = " Großes Lob",
                    Text = @"Liebes StadtWasserFluss Team,
wieder einmal haben Sie eine schwierige Situation gemeistert! Ich bin froh, dass das die Planung des Starkregenrisikomanagements in Ihren Händen liegt.

Mit freundlichen Grüßen
--
Dr.-Ing. Ella Capo
Stadtrat | Leitung der Arbeitsgruppe ""StadtWasserFluss""
Kommunales Starkregen- und Sturzfluten Management
",
                },
            };
            this.Questions = new List<Question> {
                new Question {
                    Id = 1,
                    When = "at-377",
                    Text =
                        @"Eine Starkregenwarnung wurde für das nördliche Gebiet Ihrer Kommune ausgegeben. Allerdings zeigen sich Überflutungen auch im südlichen Gebiet. Was könnte der Grund sein?",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text =
                                @"Vermutlich hat Carl mal wieder vergessen den Gartenschlauch abzustellen und hat seinen Garten überflutet.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text =
                                @"Das Wasser bewegt sich auf dem Landweg, besonders auf versiegelten Oberflächen, auf den topografisch tiefsten Punkt zu. Auch kann bei Überlastung Wasser aus dem Kanalnetz austreten.",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text = @"Dafür gibt es keinen Grund, denn das ist unmöglich.",
                            IsCorrect = false
                        },
                    },
                },
                new Question {
                    Id = 2,
                    When = null,
                    Text =
                        @"Sie haben schon einige Maßnahmen implementiert, die Öffentlichkeit informiert und die Stadt erst vor kurzem vor einem Starkregenereignis geschützt. Dennoch drängt Ihr/e Vorgesetzte/r weiter am Starkregenrisikomanagement zu arbeiten. Warum?",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text =
                                @"Risikomanagement muss als kontinuierlicher, dynamischer Prozess verstanden werden, denn Starkregenereignisse werden aufgrund des Klimawandels voraussichtlich an Häufigkeit und Intensität zunehmen.",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text = @"Genau wie Carl, will er / sie nur im Vordergrund stehen.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text =
                                @"Weil die Kommune Privatpersonen unter die Amre greift und fehlende private Maßnahmen für sie umgesetzt werden müssen.",
                            IsCorrect = false
                        },
                    },
                },
                new Question {
                    Id = 3,
                    When = null,
                    Text = @"Wenn Sie das Risiko eines Gebietes kennen, können Sie ...",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text = @"Schäden vollständig vermeiden.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text = @"Privatpersonen verpflichten ihre Raumnutzung anzupassen.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text = @"Maßnahmen priosieren.",
                            IsCorrect = true
                        },
                    },
                },
                new Question {
                    Id = 4,
                    When = null,
                    Text = @"Glauben Sie, dass baulich, technische Maßnahmen alleine ausreichen?",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text =
                                @"Ja, denn sie sind die wirkungsvollsten Maßnahmen und bieten einmal implementiert einen langfristigen Schutz.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text =
                                @"Nein, denn in  manchen Gebieten sind baulich, technische Maßnahmen nicht hinreichend umsetzbar. Hier muss verstärkt mit temporären Maßnahmen oder Verhaltens- und Nutzungsänderungen gearbeitet werden.",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text = @"Ja,denn unabhängig vom Starkregenereignis können nicht versagen.",
                            IsCorrect = false
                        },
                    },
                },
                new Question {
                    Id = 5,
                    When = null,
                    Text =
                        @"Halten Sie es für wichtig Starkregenrisikomanagement lediglich auf Quartiersebene zu entwickeln und implementieren?",
                    Answers = new List<Question.Answer> {
                        new Question.Answer {
                            Text = @"Selbstverständlich, denn hier können individuelle Potenziale genutzt werden.",
                            IsCorrect = false
                        },
                        new Question.Answer {
                            Text =
                                @"Sie bieten zwar einen Ansatz einzelne Maßnahmenkonzepte zu bündeln und aufeinander abzustimmen, dennoch umfasst ein funktionierendes Konzept die gesamte Kommune.",
                            IsCorrect = true
                        },
                        new Question.Answer {
                            Text =
                                @"Es ist sinnvoll stets auf der kleinsten möglichen Einheit zu planen. Quartiere sind daher eigentlich schon zu groß.",
                            IsCorrect = false
                        },
                    },
                },
            };
        }
    }
}
#endif