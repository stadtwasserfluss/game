#if UNITY_WEBGL
using System.Collections.Generic;

namespace Artefacts.Scripts.API.Yaml.WebGl.Actions {
    public class ActionUndergroundGarage : Action.Action {
        public ActionUndergroundGarage() {
            this.Id = "pg";
            this.HumanReadableName = "Unterirdische Parkhäuser schließen";
            this.BuildTime = 2;
            this.UsesGroundSpace = true;
            this.UsesRoofSpace = false;
            this.AreaUsage = 3000;
            this.SurvivesQuest = false;
            this.IsStackable = false;
            this.StackDelay = 0;
            this.Cost = 1;
            this.RainEffect = 20;
            this.EffectivenessPerIndex = new Dictionary<int, float> {
                {1, 1f},
                {2, 1f},
                {3, 1f},
                {4, 1f},
                {5, 1f},
                {6, 0.8f},
                {7, 0.6f}
            };
            this.LifeTime = 26;
            this.InfluencedByPublicAwareness = true;
            this.RequiresImplementationOf = new string[1] {"cs"};
            this.IsShortTerm = true;
        }
    }
}
#endif