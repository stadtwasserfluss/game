#if UNITY_WEBGL
using System.Collections.Generic;

namespace Artefacts.Scripts.API.Yaml.WebGl.Actions {
    public class ActionSewerSystem : Action.Action {
        public ActionSewerSystem() {
            this.Id = "s";
            this.HumanReadableName = "Abwassersystem erweitern";
            this.BuildTime = 90;
            this.UsesGroundSpace = true;
            this.UsesRoofSpace = false;
            this.AreaUsage = 1000;
            this.SurvivesQuest = true;
            this.IsStackable = false;
            this.StackDelay = 2;
            this.Cost = 5;
            this.RainEffect = 40;
            this.EffectivenessPerIndex = new Dictionary<int, float> {
                {1, 1f},
                {2, 1f},
                {3, 1f},
                {4, 1f},
                {5, 0.7f},
                {6, 0.4f},
                {7, 0f}
            };
            this.LifeTime = -1;
            this.InfluencedByPublicAwareness = false;
            this.RequiresImplementationOf = new string[0];
            this.IsShortTerm = false;
        }
    }
}
#endif