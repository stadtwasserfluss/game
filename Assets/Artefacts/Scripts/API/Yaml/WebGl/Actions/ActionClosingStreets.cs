#if UNITY_WEBGL
using System.Collections.Generic;

namespace Artefacts.Scripts.API.Yaml.WebGl.Actions {
    public class ActionClosingStreets : Action.Action {
        public ActionClosingStreets() {
            this.Id = "cs";
            this.HumanReadableName = "Straßen absperren";
            this.BuildTime = 1;
            this.UsesGroundSpace = true;
            this.UsesRoofSpace = false;
            this.AreaUsage = 500;
            this.SurvivesQuest = false;
            this.IsStackable = true;
            this.StackDelay = 0;
            this.Cost = 1;
            this.RainEffect = 20;
            this.EffectivenessPerIndex = new Dictionary<int, float> {
                {1, 1f},
                {2, 1f},
                {3, 0.9f},
                {4, 0.9f},
                {5, 0.8f},
                {6, 0.8f},
                {7, 0.3f}
            };
            this.LifeTime = 30;
            this.InfluencedByPublicAwareness = true;
            this.RequiresImplementationOf = new string[0];
            this.IsShortTerm = true;
        }
    }
}
#endif