#if UNITY_WEBGL
using System.Collections.Generic;

namespace Artefacts.Scripts.API.Yaml.WebGl.Actions {
    public class ActionMultipurposeArea : Action.Action {
        public ActionMultipurposeArea() {
            this.Id = "m";
            this.HumanReadableName = "Multifunktionale Flächen erstellen";
            this.BuildTime = 70;
            this.UsesGroundSpace = true;
            this.UsesRoofSpace = false;
            this.AreaUsage = 1000;
            this.SurvivesQuest = true;
            this.IsStackable = true;
            this.StackDelay = 7;
            this.Cost = 4;
            this.RainEffect = 50;
            this.EffectivenessPerIndex = new Dictionary<int, float> {
                {1, 1f},
                {2, 1f},
                {3, 1f},
                {4, 1f},
                {5, 1f},
                {6, 1f},
                {7, 0.8f}
            };
            this.LifeTime = -1;
            this.InfluencedByPublicAwareness = true;
            this.RequiresImplementationOf = new string[0];
            this.IsShortTerm = false;
        }
    }
}
#endif