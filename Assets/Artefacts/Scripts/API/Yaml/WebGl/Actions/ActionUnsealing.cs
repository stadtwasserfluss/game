#if UNITY_WEBGL
using System.Collections.Generic;

namespace Artefacts.Scripts.API.Yaml.WebGl.Actions {
    public class ActionUnsealing : Action.Action {
        public ActionUnsealing() {
            this.Id = "u";
            this.HumanReadableName = "Flächen entsiegeln";
            this.BuildTime = 7;
            this.UsesGroundSpace = true;
            this.UsesRoofSpace = false;
            this.AreaUsage = 500;
            this.SurvivesQuest = true;
            this.IsStackable = true;
            this.StackDelay = 2;
            this.Cost = 2;
            this.RainEffect = 20;
            this.EffectivenessPerIndex = new Dictionary<int, float> {
                {1, 1f},
                {2, 1f},
                {3, 0.95f},
                {4, 0.8f},
                {5, 0.6f},
                {6, 0.4f},
                {7, 0.2f}
            };
            this.LifeTime = -1;
            this.InfluencedByPublicAwareness = false;
            this.RequiresImplementationOf = new string[0];
            this.IsShortTerm = false;
        }
    }
}
#endif