#if UNITY_WEBGL
using System.Collections.Generic;

namespace Artefacts.Scripts.API.Yaml.WebGl.Actions {
    public class ActionSandbags : Action.Action {
        public ActionSandbags() {
            this.Id = "sb";
            this.HumanReadableName = "Sandsäcke verteilen";
            this.BuildTime = 1;
            this.UsesGroundSpace = true;
            this.UsesRoofSpace = false;
            this.AreaUsage = 20;
            this.SurvivesQuest = false;
            this.IsStackable = true;
            this.StackDelay = 0;
            this.Cost = 2;
            this.RainEffect = 20;
            this.EffectivenessPerIndex = new Dictionary<int, float> {
                {1, 1f},
                {2, 1f},
                {3, 1f},
                {4, 1f},
                {5, 1f},
                {6, 0f},
                {7, 0f}
            };
            this.LifeTime = 26;
            this.InfluencedByPublicAwareness = true;
            this.RequiresImplementationOf = new string[0];
            this.IsShortTerm = true;
        }
    }
}
#endif