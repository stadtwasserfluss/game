#if UNITY_WEBGL
using System.Collections.Generic;

namespace Artefacts.Scripts.API.Yaml.WebGl.Actions {
    public class ActionGreenRoof : Action.Action {
        public ActionGreenRoof() {
            this.Id = "g";
            this.HumanReadableName = "Gründächer bauen";
            this.BuildTime = 60;
            this.UsesGroundSpace = false;
            this.UsesRoofSpace = true;
            this.AreaUsage = 2000;
            this.SurvivesQuest = true;
            this.IsStackable = true;
            this.StackDelay = 7;
            this.Cost = 1;
            this.RainEffect = 10;
            this.EffectivenessPerIndex = new Dictionary<int, float> {
                {1, 1f},
                {2, 1f},
                {3, 0.7f},
                {4, 0.7f},
                {5, 0.5f},
                {6, 0.1f},
                {7, 0.1f}
            };
            this.LifeTime = -1;
            this.InfluencedByPublicAwareness = false;
            this.RequiresImplementationOf = new string[0];
            this.IsShortTerm = false;
        }
    }
}
#endif