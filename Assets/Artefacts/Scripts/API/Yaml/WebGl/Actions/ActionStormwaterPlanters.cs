#if UNITY_WEBGL
using System.Collections.Generic;

namespace Artefacts.Scripts.API.Yaml.WebGl.Actions {
    public class ActionStormwaterPlanters : Action.Action {
        public ActionStormwaterPlanters() {
            this.Id = "p";
            this.HumanReadableName = "Retentionstiefbeete anlegen";
            this.BuildTime = 10;
            this.UsesGroundSpace = true;
            this.UsesRoofSpace = false;
            this.AreaUsage = 50;
            this.SurvivesQuest = true;
            this.IsStackable = true;
            this.StackDelay = 2;
            this.Cost = 3;
            this.RainEffect = 30;
            this.EffectivenessPerIndex = new Dictionary<int, float> {
                {1, 1f},
                {2, 1f},
                {3, 1f},
                {4, 1f},
                {5, 0.8f},
                {6, 0.5f},
                {7, 0.3f}
            };
            this.LifeTime = -1;
            this.InfluencedByPublicAwareness = false;
            this.RequiresImplementationOf = new string[0];
            this.IsShortTerm = false;
        }
    }
}
#endif