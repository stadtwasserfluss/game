#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector03 : Sector {
        public Sector03() {
            this.Id = 3;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "u", "p", "cs", "pg", "mg"};
            this.AreaSector = 87255;
            this.AvailableGroundArea = 32627;
            this.AvailableRoofArea = 8644;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 3},
                {"cs", 1},
                {"m", 0},
                {"sb", 0},
                {"s", 0},
                {"p", 6},
                {"pg", 0},
                {"u", 9}
            };
        }
    }
}
#endif