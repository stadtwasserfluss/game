#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector09 : Sector {
        public Sector09() {
            this.Id = 9;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "u", "p", "cs"};
            this.AreaSector = 161805;
            this.AvailableGroundArea = 72260;
            this.AvailableRoofArea = 24503;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 8},
                {"cs", 1},
                {"m", 0},
                {"sb", 1},
                {"s", 0},
                {"p", 12},
                {"pg", 0},
                {"u", 7}
            };
        }
    }
}
#endif