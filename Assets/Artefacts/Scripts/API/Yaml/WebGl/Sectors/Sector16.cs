#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector16 : Sector {
        public Sector16() {
            this.Id = 16;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "p", "sb", "cs", "mg"};
            this.AreaSector = 340305;
            this.AvailableGroundArea = 168694;
            this.AvailableRoofArea = 31351;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 15},
                {"cs", 0},
                {"m", 0},
                {"sb", 0},
                {"s", 0},
                {"p", 0},
                {"pg", 0},
                {"u", 0}
            };
        }
    }
}
#endif