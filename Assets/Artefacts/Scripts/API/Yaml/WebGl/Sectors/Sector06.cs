#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector06 : Sector {
        public Sector06() {
            this.Id = 6;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "u", "p", "pg", "mg", "cs", "sb"};
            this.AreaSector = 122819;
            this.AvailableGroundArea = 38200;
            this.AvailableRoofArea = 16928;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 3},
                {"cs", 2},
                {"m", 0},
                {"sb", 0},
                {"s", 0},
                {"p", 8},
                {"pg", 2},
                {"u", 6}
            };
        }
    }
}
#endif