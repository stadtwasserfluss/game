#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector13 : Sector {
        public Sector13() {
            this.Id = 13;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "m", "p", "sb", "mg"};
            this.AreaSector = 127263;
            this.AvailableGroundArea = 2708;
            this.AvailableRoofArea = 91985;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 1},
                {"cs", 0},
                {"m", 4},
                {"sb", 8},
                {"s", 2},
                {"p", 8},
                {"pg", 0},
                {"u", 2}
            };
        }
    }
}
#endif