#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector11 : Sector {
        public Sector11() {
            this.Id = 11;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "u", "p", "sb", "mg"};
            this.AreaSector = 131876;
            this.AvailableGroundArea = 64713;
            this.AvailableRoofArea = 21059;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 8},
                {"cs", 1},
                {"m", 2},
                {"sb", 0},
                {"s", 0},
                {"p", 7},
                {"pg", 0},
                {"u", 0}
            };
        }
    }
}
#endif