#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector10 : Sector {
        public Sector10() {
            this.Id = 10;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "m", "p", "s", "pg", "u", "cs", "sb"};
            this.AreaSector = 150316;
            this.AvailableGroundArea = 72712;
            this.AvailableRoofArea = 13363;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 4},
                {"cs", 1},
                {"m", 1},
                {"sb", 10},
                {"s", 7},
                {"p", 10},
                {"pg", 1},
                {"u", 7}
            };
        }
    }
}
#endif