#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector04 : Sector {
        public Sector04() {
            this.Id = 4;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "p", "cs"};
            this.AreaSector = 104823;
            this.AvailableGroundArea = 44123;
            this.AvailableRoofArea = 2415;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 1},
                {"cs", 1},
                {"m", 0},
                {"sb", 0},
                {"s", 0},
                {"p", 8},
                {"pg", 0},
                {"u", 0}
            };
        }
    }
}
#endif