#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector05 : Sector {
        public Sector05() {
            this.Id = 5;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "u", "m", "p", "cs", "pg", "mg"};
            this.AreaSector = 136591;
            this.AvailableGroundArea = 56934;
            this.AvailableRoofArea = 22252;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 5},
                {"cs", 2},
                {"m", 2},
                {"sb", 0},
                {"s", 0},
                {"p", 10},
                {"pg", 1},
                {"u", 5}
            };
        }
    }
}
#endif