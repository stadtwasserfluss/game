#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector14 : Sector {
        public Sector14() {
            this.Id = 14;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "m", "p", "sb", "u"};
            this.AreaSector = 250653;
            this.AvailableGroundArea = 123273;
            this.AvailableRoofArea = 27995;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 9},
                {"cs", 0},
                {"m", 3},
                {"sb", 7},
                {"s", 0},
                {"p", 6},
                {"pg", 0},
                {"u", 4}
            };
        }
    }
}
#endif