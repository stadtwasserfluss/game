#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector02 : Sector {
        public Sector02() {
            this.Id = 2;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "u", "m", "p", "cs"};
            this.AreaSector = 58543;
            this.AvailableGroundArea = 11877;
            this.AvailableRoofArea = 4716;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 1},
                {"cs", 1},
                {"m", 2},
                {"sb", 0},
                {"s", 0},
                {"p", 9},
                {"pg", 0},
                {"u", 6}
            };
        }
    }
}
#endif