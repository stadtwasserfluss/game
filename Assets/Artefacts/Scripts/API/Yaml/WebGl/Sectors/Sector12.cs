#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector12 : Sector {
        public Sector12() {
            this.Id = 12;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "u", "p", "sb", "cs", "mg"};
            this.AreaSector = 161547;
            this.AvailableGroundArea = 88035;
            this.AvailableRoofArea = 24013;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 8},
                {"cs", 0},
                {"m", 0},
                {"sb", 0},
                {"s", 0},
                {"p", 10},
                {"pg", 0},
                {"u", 1}
            };
        }
    }
}
#endif