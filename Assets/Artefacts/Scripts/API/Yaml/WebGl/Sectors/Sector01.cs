#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector01 : Sector {
        public Sector01() {
            this.Id = 1;
            this.PublicAwarenessFactor = 0.24f;
            this.AvailableActions = new[] {"g", "sb", "u", "p"};
            this.AreaSector = 41834;
            this.AvailableGroundArea = 16831;
            this.AvailableRoofArea = 2802;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 1},
                {"cs", 0},
                {"m", 0},
                {"sb", 12},
                {"s", 0},
                {"p", 6},
                {"pg", 0},
                {"u", 5}
            };
        }
    }
}
#endif