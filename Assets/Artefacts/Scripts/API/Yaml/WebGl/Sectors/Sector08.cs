#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector08 : Sector {
        public Sector08() {
            this.Id = 8;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "u", "m", "p", "sb", "cs", "pg", "mg"};
            this.AreaSector = 142468;
            this.AvailableGroundArea = 40394;
            this.AvailableRoofArea = 27458;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 10},
                {"cs", 4},
                {"m", 2},
                {"sb", 10},
                {"s", 0},
                {"p", 8},
                {"pg", 1},
                {"u", 6}
            };
        }
    }
}
#endif