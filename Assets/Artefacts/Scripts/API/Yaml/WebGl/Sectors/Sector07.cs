#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector07 : Sector {
        public Sector07() {
            this.Id = 6;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "p", "sb", "cs", "pg", "mg"};
            this.AreaSector = 88790;
            this.AvailableGroundArea = 18571;
            this.AvailableRoofArea = 18610;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 8},
                {"cs", 2},
                {"m", 0},
                {"sb", 10},
                {"s", 0},
                {"p", 8},
                {"pg", 2},
                {"u", 6}
            };
        }
    }
}
#endif