#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Map;

namespace Artefacts.Scripts.API.Yaml.WebGl.Sectors {
    public class Sector15 : Sector {
        public Sector15() {
            this.Id = 15;
            this.PublicAwarenessFactor = 0f;
            this.AvailableActions = new[] {"g", "p", "m", "cs", "u"};
            this.AreaSector = 340305;
            this.AvailableGroundArea = 168694;
            this.AvailableRoofArea = 27995;
            this.MaxStack = new Dictionary<string, int> {
                {"g", 7},
                {"cs", 2},
                {"m", 1},
                {"sb", 3},
                {"s", 0},
                {"p", 7},
                {"pg", 0},
                {"u", 4}
            };
        }
    }
}
#endif