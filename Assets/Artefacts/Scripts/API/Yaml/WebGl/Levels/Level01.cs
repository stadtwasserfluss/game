#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Gameplay;

namespace Artefacts.Scripts.API.Yaml.WebGl.Levels {
    public class Level01 : Level {
        public Level01() {
            this.Id = 1;
            this.Next = 2;
            this.EventAt = 94;
            this.EventDuration = 3;
            this.PreparationTime = 24;
            this.FastForwardTimeFactor = 15.0f;
            this.AdditionalMoney = 2500;
            this.AffectedSectors = new[] {10};
            this.ActiveSectors = new[] {10};
            this.CameraIsLocked = true;
            this.MinimapIsClickable = false;
            this.EnabledActions = new[] {"g", "pg", "cs", "sb"};
            this.RainIndex = 2;
            this.PreventedDamageRatingThresholds = new[] {0.1f, 0.2f, 0.3f};
            this.PublicAwarenessConfiguration = new Level01PaConfiguration();
            this.BoostFromPrivateActions = 0.3f;
            this.RainIndexDistribution = new Dictionary<int, float> {{10, 1f}};
            this.SectorImportance = new Dictionary<int, float> {{10, 1f}};
        }

        private class Level01PaConfiguration : PaConfiguration {
            public Level01PaConfiguration() {
                this.InitialPublicAwareness = 0.0f;
                this.Display = new Level01PaConfigurationPaDisplay();
                this.Button = new Level01PaConfigurationPaButton();
                this.QuizBonus = 0.2f;
            }

            private class Level01PaConfigurationPaDisplay : PaDisplay {
                public Level01PaConfigurationPaDisplay() {
                    this.DecreaseWithTime = 0.2f;
                    this.TimeStepsForDecrease = 14;
                }
            }

            private class Level01PaConfigurationPaButton : PaButton {
                public Level01PaConfigurationPaButton() {
                    this.OnClickRaiseBy = 1.0f;
                    this.OnClickPenaltyDecreaseBy = 0.3f;
                    this.PenaltyTimeSteps = 17;
                }
            }
        }
    }
}
#endif