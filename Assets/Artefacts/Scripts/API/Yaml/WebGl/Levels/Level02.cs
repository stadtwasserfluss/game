#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Gameplay;

namespace Artefacts.Scripts.API.Yaml.WebGl.Levels {
    public class Level02 : Level {
        public Level02() {
            this.Id = 2;
            this.Next = 3;
            this.EventAt = 124;
            this.EventDuration = 3;
            this.PreparationTime = 24;
            this.FastForwardTimeFactor = 15.0f;
            this.AdditionalMoney = 7000;
            this.AffectedSectors = new[] {6, 10, 13};
            this.ActiveSectors = new[] {6, 10, 13};
            this.CameraIsLocked = false;
            this.MinimapIsClickable = true;
            this.EnabledActions = new[] {"g", "m", "p", "s", "pg", "u", "cs", "sb"};
            this.RainIndex = 6;
            this.PreventedDamageRatingThresholds = new[] {0.3f, 0.5f, 0.75f};
            this.PublicAwarenessConfiguration = new Level02PaConfiguration();
            this.BoostFromPrivateActions = 0.3f;
            this.RainIndexDistribution = new Dictionary<int, float> {{6, 0.2f}, {10, 0.3f}, {13, 0.5f}};
            this.SectorImportance = new Dictionary<int, float> {{6, 0.6f}, {10, 0.3f}, {13, 0.1f}};
        }

        private class Level02PaConfiguration : PaConfiguration {
            public Level02PaConfiguration() {
                this.InitialPublicAwareness = 0.0f;
                this.Display = new Level02PaConfigurationPaDisplay();
                this.Button = new Level02PaConfigurationPaButton();
                this.QuizBonus = 0.2f;
            }

            private class Level02PaConfigurationPaDisplay : PaDisplay {
                public Level02PaConfigurationPaDisplay() {
                    this.DecreaseWithTime = 0.2f;
                    this.TimeStepsForDecrease = 14;
                }
            }

            private class Level02PaConfigurationPaButton : PaButton {
                public Level02PaConfigurationPaButton() {
                    this.OnClickRaiseBy = 1.0f;
                    this.OnClickPenaltyDecreaseBy = 0.3f;
                    this.PenaltyTimeSteps = 17;
                }
            }
        }
    }
}
#endif