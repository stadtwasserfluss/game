#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Gameplay;

namespace Artefacts.Scripts.API.Yaml.WebGl.Levels {
    public class Level03 : Level {
        public Level03() {
            this.Id = 3;
            this.Next = -1;
            this.EventAt = 174;
            this.EventDuration = 3;
            this.PreparationTime = 24;
            this.FastForwardTimeFactor = 15.0f;
            this.AdditionalMoney = 15000;
            this.AffectedSectors = new[] {1, 2, 3, 4, 5, 7, 15};
            this.ActiveSectors = new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
            this.CameraIsLocked = false;
            this.MinimapIsClickable = true;
            this.EnabledActions = new[] {"g", "m", "p", "s", "pg", "u", "cs", "sb"};
            this.RainIndex = 8;
            this.PreventedDamageRatingThresholds = new[] {0.3f, 0.5f, 0.75f};
            this.PublicAwarenessConfiguration = new Level03PaConfiguration();
            this.BoostFromPrivateActions = 0.4f;
            this.RainIndexDistribution = new Dictionary<int, float> {
                {1, 0.2f}, {2, 0.2f}, {3, 0.1f}, {4, 0.1f}, {5, 0.1f}, {6, 0.0f}, {7, 0.2f}, {8, 0.0f}, {9, 0.0f},
                {10, 0.0f}, {11, 0.0f}, {12, 0.0f}, {13, 0.0f}, {14, 0.0f}, {15, 0.1f}, {16, 0.0f}
            };
            this.SectorImportance = new Dictionary<int, float> {
                {1, 0.3f}, {2, 0.2f}, {3, 0.1f}, {4, 0.1f}, {5, 0.1f}, {6, 0.0f}, {7, 0.1f}, {8, 0.0f}, {9, 0.0f},
                {10, 0.0f}, {11, 0.0f}, {12, 0.0f}, {13, 0.0f}, {14, 0.0f}, {15, 0.1f}, {16, 0.0f}
            };
        }

        private class Level03PaConfiguration : PaConfiguration {
            public Level03PaConfiguration() {
                this.InitialPublicAwareness = 0.0f;
                this.Display = new Level03PaConfigurationPaDisplay();
                this.Button = new Level03PaConfigurationPaButton();
                this.QuizBonus = 0.2f;
            }

            private class Level03PaConfigurationPaDisplay : PaDisplay {
                public Level03PaConfigurationPaDisplay() {
                    this.DecreaseWithTime = 0.2f;
                    this.TimeStepsForDecrease = 14;
                }
            }

            private class Level03PaConfigurationPaButton : PaButton {
                public Level03PaConfigurationPaButton() {
                    this.OnClickRaiseBy = 1.0f;
                    this.OnClickPenaltyDecreaseBy = 0.3f;
                    this.PenaltyTimeSteps = 17;
                }
            }
        }
    }
}
#endif