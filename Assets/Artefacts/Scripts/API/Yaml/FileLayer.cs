using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Artefacts.Scripts.API.Yaml {
    public static class FileLayer {
        public static IEnumerable<string> GetFilesInDirectory(string relative,
                                                              IEnumerable<string> allowedExtensions = null) {
            string rootDirectory = FileLayer.GetRootDirectory();

            var files = Directory.GetFiles(Path.Combine(rootDirectory, relative))
               .Where(s => allowedExtensions == null || allowedExtensions.Contains(Path.GetExtension(s)));

            files = from f in files where f.StartsWith(rootDirectory) select f.Substring(rootDirectory.Length);

            return files;
        }

        public static IEnumerable<string> GetYamlFilesInDirectory(string relative) {
#if UNITY_ANDROID && !UNITY_EDITOR
            BetterStreamingAssets.Initialize();
            return BetterStreamingAssets.GetFiles(relative + "/", "*.yaml", SearchOption.AllDirectories);
#else
            return FileLayer.GetFilesInDirectory(relative, new[] {".yaml"});
#endif
        }

        public static string GetRootDirectory() {
            return Application.streamingAssetsPath + "/";
        }
    }
}