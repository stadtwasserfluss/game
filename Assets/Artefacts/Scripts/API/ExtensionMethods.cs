using System.Collections;
using System.Collections.Generic;

namespace Artefacts.Scripts.API {
    public static class ExtensionMethods {
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key,
                                                        TValue defaultValue) {
            return dictionary.TryGetValue(key, out var value) ? value : defaultValue;
        }

        public static float GetOrZero<TKey>(this IDictionary<TKey, float> dictionary, TKey key) {
            return dictionary.GetOrDefault(key, 0f);
        }

        public static int GetOrZero<TKey>(this IDictionary<TKey, int> dictionary, TKey key) {
            return dictionary.GetOrDefault(key, 0);
        }

        public static float GetOrOne<TKey>(this IDictionary<TKey, float> dictionary, TKey key) {
            return dictionary.GetOrDefault(key, 1f);
        }

        public static void RemoveAll<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, IEnumerable<TKey> keys) {
            foreach (var key in keys) {
                if (!dictionary.ContainsKey(key)) continue;

                dictionary.Remove(key);
            }
        }

        public static bool IsEmpty(this ICollection collection) {
            return collection.Count == 0;
        }
    }
}