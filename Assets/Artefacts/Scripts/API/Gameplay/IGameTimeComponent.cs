namespace Artefacts.Scripts.API.Gameplay {
    public interface IGameTimeComponent {
        void BeforeTimeStep(long now);
        void OnTimeStep(long now);
        void AfterTimeStep(long now);
    }
}