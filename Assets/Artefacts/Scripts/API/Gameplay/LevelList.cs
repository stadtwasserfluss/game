#if UNITY_WEBGL
using Artefacts.Scripts.API.Yaml.WebGl.Levels;
#else
using Artefacts.Scripts.API.Yaml;
#endif
using System;
using System.Collections.Generic;
using System.Linq;

namespace Artefacts.Scripts.API.Gameplay {
    public sealed class LevelList {
        private static readonly Lazy<LevelList> LazyInstance = new Lazy<LevelList>(() => new LevelList());
        private IEnumerable<Level> _availableLevels;

        private LevelList() {
            this.Initialize();
        }

        public static LevelList Instance => LevelList.LazyInstance.Value;

        public Level GetFirst() {
            return this._availableLevels.First(al => al.Id == this._availableLevels.Min(l => l.Id));
        }

        public Level GetNextLevel(Level previous) {
            return this._availableLevels.FirstOrDefault(al => al.Id == previous.Next);
        }

        private void Initialize() {
#if UNITY_WEBGL
            this._availableLevels = new Level[] {
                new Level01(),
                new Level02(),
                new Level03()
            };
#else
            this._availableLevels =
                from f in FileLayer.GetYamlFilesInDirectory("data/levels")
                select YamlLoader<Level>.Load(f);
#endif
        }
    }
}