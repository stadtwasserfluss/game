using System;
using System.Collections.Generic;
using System.Linq;
using Artefacts.Scripts.API.Catastrophe;
using Artefacts.Scripts.API.Map;
using Artefacts.Scripts.API.Notification;
using JetBrains.Annotations;

namespace Artefacts.Scripts.API.Gameplay {
    public sealed class EventManager : IGameTimeComponent {
        private static Lazy<EventManager> _lazyInstance = EventManager.GetLazyInstance();
        private readonly List<DelayedNotification> _delayedNotifications;
        private readonly List<Rain> _pendingCatastrophes;

        private long _currentTimeStep;

        private EventManager() {
            this._pendingCatastrophes = new List<Rain>();
            this._delayedNotifications = new List<DelayedNotification>();
            this.Catastrophes = new List<Rain>();
            this.Notifications = new List<Notification.Notification>();

            GameTime.I.AddComponent(this);
        }

        public List<Rain> Catastrophes { get; }
        public List<Notification.Notification> Notifications { get; }

        public static EventManager I => EventManager._lazyInstance.Value;

        public bool IsInPreparationPhase => this.GetCurrentCatastropheNotifications().Any();

        public void BeforeTimeStep(long now) {
            this._currentTimeStep = now;

            this.ExposeCurrent();
            this.RemoveOld();
        }

        public void OnTimeStep(long now) { }

        public void AfterTimeStep(long now) {
            if (!GameManager.I.CurrentLevel.EventLoaded) this.LoadNewEvent();
        }

        public void AddNotification(Notification.Notification notification) {
            this.Notifications.Add(notification);
        }

        public void AddNotification(DelayedNotification notification) {
            this._delayedNotifications.Add(notification);
        }

        public IEnumerable<CatastropheNotification> GetCurrentCatastropheNotifications() {
            return this.Notifications.OfType<CatastropheNotification>();
        }

        [CanBeNull]
        public CatastropheNotification GetCurrentCatastropheNotification() {
            return (from cn in this.GetCurrentCatastropheNotifications()
                    where cn.StartAt == this.GetCurrentCatastropheNotifications().Min(n => n.StartAt)
                    select cn).FirstOrDefault();
        }

        public bool IsSectorAffectedByNextCatastrophe(Sector sector) {
            return sector != null &&
                   (this.GetCurrentCatastropheNotification()?.Rain.AffectedObjects.Contains(sector) ?? false);
        }

        public static void Reset() {
            EventManager._lazyInstance = EventManager.GetLazyInstance();
        }

        private static Lazy<EventManager> GetLazyInstance() {
            return new Lazy<EventManager>(() => new EventManager());
        }

        private void AddCatastrophe(Rain rain) {
            this._pendingCatastrophes.Add(rain);
        }

        private void LoadNewEvent() {
            var gm = GameManager.I;

            var map = gm.Map;
            var rain = new Rain(gm.CurrentLevel.RainIndex);

            rain.AddAffectedObjects(map.AllSectors.Where(s => gm.CurrentLevel.AffectedSectors.Contains(s.Id)));
            this.AddCatastrophe(rain);

            gm.CurrentLevel.EventLoaded = true;
        }

        private void ExposeCurrent() {
            this._pendingCatastrophes
               .FindAll(e => e.StartAt <= this._currentTimeStep)
               .ForEach(this.Catastrophes.Add);

            // Remove pending events from the list
            this._pendingCatastrophes.RemoveAll(this.Catastrophes.Contains);

            this._delayedNotifications
               .FindAll(n => n.StartAt <= this._currentTimeStep)
               .ForEach(this.Notifications.Add);

            this._delayedNotifications.RemoveAll(this.Notifications.Contains);
        }

        private void RemoveOld() {
            this.Catastrophes.RemoveAll(e => this._currentTimeStep > e.EndAt);
            this.Notifications.RemoveAll(n => n.DurationLeft < 1);
        }
    }
}