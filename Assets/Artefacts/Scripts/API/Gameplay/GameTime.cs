using System;
using System.Collections.Generic;

namespace Artefacts.Scripts.API.Gameplay {
    public sealed class GameTime {
        public enum TimeFormat {
            AsDuration = 0, OnlyDays = 1, Hours = 1 << 1
        }

        private static Lazy<GameTime> _lazyInstance = GameTime.GetLazyInstance();
        private readonly List<IGameTimeComponent> _affectedComponents;
        private long _currentElapsedMillis;
        private float _factor;
        private long _millisPerTimeStep;

        private GameTime() {
            this._affectedComponents = new List<IGameTimeComponent>();
            this._factor = 1.0f;
            this._millisPerTimeStep = 5000;
            this.IsPaused = false;
            this._currentElapsedMillis = 0;
        }

        public bool IsPaused { get; private set; }

        public bool IsSpedUp => this._factor > 1;

        public long Now { get; private set; }

        public string FormatAsDays => GameTime.FormatTime(this.Now);

        public string FormatAsHours => GameTime.FormatTime(this.Now, TimeFormat.Hours);

        public static GameTime I => GameTime._lazyInstance.Value;

        public void AddComponent(IGameTimeComponent component) {
            if (!this._affectedComponents.Contains(component)) this._affectedComponents.Add(component);
        }

        public void AddComponents(params IGameTimeComponent[] components) {
            foreach (var component in components) this.AddComponent(component);
        }

        public void SetSpeedup(float factor) {
            if (factor < 0.1f)
                throw new ArgumentException("Prevented setting time factor to near zero or negative.");

            this._factor = factor;
        }

        public void SetMillisecondsPerTimeStep(int millis) {
            this._millisPerTimeStep = millis;
        }

        public void Tick(long elapsedTimeMillis) {
            // Do setup work on first tick only
            if (this.Now == 0 && this._currentElapsedMillis == 0) {
                this.SendTimeStepToComponents();
                this.Now = 1;
                this.SendTimeStepToComponents();
            }

            if (this.IsPaused) return;

            this._currentElapsedMillis += (long) (elapsedTimeMillis * this._factor);

            if (this._currentElapsedMillis < this._millisPerTimeStep) return;

            this.Now++;
            this._currentElapsedMillis -= this._millisPerTimeStep;

            this.SendTimeStepToComponents();
        }

        public static void Reset() {
            GameTime._lazyInstance = GameTime.GetLazyInstance();
        }

        private static Lazy<GameTime> GetLazyInstance() {
            return new Lazy<GameTime>(() => new GameTime());
        }

        private void SendTimeStepToComponents() {
            // For loop needed because it is possible that new components are registered within the OnTimeStep functions
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < this._affectedComponents.Count; i++)
                this._affectedComponents[i].BeforeTimeStep(this.Now);

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < this._affectedComponents.Count; i++)
                this._affectedComponents[i].OnTimeStep(this.Now);

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < this._affectedComponents.Count; i++)
                this._affectedComponents[i].AfterTimeStep(this.Now);

            GameManager.I.CurrentStory.Notify($"at-{this.Now}");
        }

        public void Pause(bool notify = true) {
            this.IsPaused = true;
            this.SetSpeedup(1);

            if (notify)
                GameManager.I.CurrentStory.Notify("game-paused");
        }

        public void Unpause(bool notify = true) {
            this.IsPaused = false;

            if (notify)
                GameManager.I.CurrentStory.Notify("game-continued");
        }

        public static string FormatTime(long timeSteps, TimeFormat format = TimeFormat.OnlyDays) {
            switch (format) {
                case TimeFormat.AsDuration:
                    return $"{timeSteps} Tage";
                case TimeFormat.OnlyDays:
                    return $"Tag {timeSteps}";
                case TimeFormat.Hours:
                    return $"{timeSteps % 24}:00h";
                default:
                    throw new ArgumentOutOfRangeException(nameof(format), format, null);
            }
        }
    }
}