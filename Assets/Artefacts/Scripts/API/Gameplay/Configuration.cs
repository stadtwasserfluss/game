// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Artefacts.Scripts.API.Gameplay {
    public class Configuration {
        public int GameStepDuration { get; protected set; }
        public bool PauseAtNewLevel { get; protected set; }
        public bool StartGamePaused { get; protected set; }
    }
}