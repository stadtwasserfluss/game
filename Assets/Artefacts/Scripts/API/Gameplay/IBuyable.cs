namespace Artefacts.Scripts.API.Gameplay {
    public interface IBuyable {
        int Cost { get; }
    }
}