using System;
using System.Collections.Generic;
using System.Linq;

namespace Artefacts.Scripts.API.Gameplay {
    public sealed class BankAccount : IGameTimeComponent {
        private static Lazy<BankAccount> _lazyInstance = BankAccount.GetLazyInstance();
        private readonly List<Sale> _saleHistory;

        private BankAccount() {
            this._saleHistory = new List<Sale>();

            GameTime.I.AddComponent(this);
        }

        public int Saldo { get; set; }
        public IEnumerable<Sale> SaleHistory => this._saleHistory;
        public int TotalSpendings => this._saleHistory.Sum(s => s.Subtotal);

        public static BankAccount Instance => BankAccount._lazyInstance.Value;

        public void BeforeTimeStep(long now) { }

        public void OnTimeStep(long now) { }

        public void AfterTimeStep(long now) {
            var gameManager = GameManager.I;

            this.AddInterest(gameManager.CurrentLevel.Interest);
        }

        public void Buy(IBuyable item, float quantity = 1f) {
            var sale = new Sale(item, quantity);
            this._saleHistory.Add(sale);
            this.Saldo -= sale.Subtotal;
        }

        public bool CanBuy(IBuyable item, float quantity = 1f) {
            return new Sale(item, quantity).Subtotal <= this.Saldo;
        }

        public static void Reset() {
            BankAccount._lazyInstance = BankAccount.GetLazyInstance();
        }

        private static Lazy<BankAccount> GetLazyInstance() {
            return new Lazy<BankAccount>(() => new BankAccount());
        }

        private void AddInterest(float percent) {
            this.Saldo += (int) (this.Saldo * percent / 100);
        }


        public readonly struct Sale {
            public readonly IBuyable Item;
            public readonly float Quantity;

            public Sale(IBuyable item, float quantity) {
                this.Item = item;
                this.Quantity = quantity;
            }

            public int Subtotal => (int) Math.Round(this.Item.Cost * this.Quantity);
        }
    }
}