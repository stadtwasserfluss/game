#if UNITY_WEBGL
using Artefacts.Scripts.API.Yaml.WebGl;
#else
using Artefacts.Scripts.API.Yaml;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using Artefacts.Scripts.API.Action;
using Artefacts.Scripts.API.Catastrophe;
using Artefacts.Scripts.API.Communication;
using Artefacts.Scripts.API.Map;
using Artefacts.Scripts.API.Notification;

namespace Artefacts.Scripts.API.Gameplay {
    public sealed class GameManager : IGameTimeComponent {
        public enum GameStates {
            Running = 0, Ended = 1
        }

        private static Lazy<GameManager> _lazyInstance = GameManager.GetLazyInstance();
        private readonly bool _controlTiming;
        private bool _loadNextLevel;
        private SectorMap _map;

        private GameManager() {
            this.CurrentEvents = new List<Rain>();
            this.CurrentNotifications = new List<Notification.Notification>();
#if UNITY_WEBGL
            this.Configuration = new GameConfiguration();
#else
            this.Configuration = YamlLoader<Configuration>.Load("data/game.yaml");
#endif
            this.AvailableActions = ActionList.I;
            this.GameState = GameStates.Running;

            var gt = GameTime.I;
            gt.AddComponent(this);
            gt.SetMillisecondsPerTimeStep(this.Configuration.GameStepDuration);

            var eventManager = EventManager.I;

            // Link notifications list
            this.CurrentNotifications = eventManager.Notifications;

            // Link events list
            this.CurrentEvents = eventManager.Catastrophes;

            // Load first level
            this.LoadLevel();

            // Start game paused
            if (this.Configuration.StartGamePaused)
                GameTime.I.Pause(false);
        }

        public Level CurrentLevel { get; private set; }
        public List<Rain> CurrentEvents { get; }
        public List<Notification.Notification> CurrentNotifications { get; }
        public ActionList AvailableActions { get; }
        public GameStates GameState { get; private set; }
        public Story CurrentStory { get; private set; }

        public SectorMap Map {
            get {
                if (this._map == null) throw new Exception("Map is not set. Cannot be accessed");

                return this._map;
            }
        }

        public Configuration Configuration { get; }

        public static GameManager I => GameManager._lazyInstance.Value;

        public void BeforeTimeStep(long now) {
            if (this._loadNextLevel)
                this.NextLevel();

            if (!EventManager.I.IsInPreparationPhase)
                return;

            GameTime.I.SetSpeedup(1);
            this.CurrentStory?.Notify("preparation-start");
        }

        public void OnTimeStep(long now) {
            this.CurrentStory?.Notify($"at-{now}");
        }

        public void AfterTimeStep(long now) {
            if (!this.CurrentNotifications.OfType<CatastrophePassedNotification>().Any()) {
                this._loadNextLevel = false;
                return;
            }

            this.CurrentStory?.Notify("rain-end");
            this._loadNextLevel = true;
        }

        public void SetMap(SectorMap map) {
            this._map = map;
        }

        public static void Reset() {
            GameManager._lazyInstance = GameManager.GetLazyInstance();
        }

        private static Lazy<GameManager> GetLazyInstance() {
            return new Lazy<GameManager>(() => new GameManager());
        }

        private void LoadLevel() {
            // Get next level
            var ll = LevelList.Instance;
            var level = this.CurrentLevel == null ? ll.GetFirst() : ll.GetNextLevel(this.CurrentLevel);

            // End the game if no further level is available
            if (level == null) {
                this.GameState = GameStates.Ended;
                return;
            }

            // Switch level
            this.CurrentLevel = level;

            // Add money
            BankAccount.Instance.Saldo += level.AdditionalMoney;

            // Switch story
            this.CurrentStory = StoryProvider.Instance.GetForLevel(level);
        }

        private void NextLevel() {
            this.LoadLevel();

            // Start level in paused state
            if (this.Configuration.PauseAtNewLevel)
                GameTime.I.Pause();

            // Clear all events
            this.CurrentEvents.RemoveRange(0, this.CurrentEvents.Count);

            // Clear all non-quest surving actions
            foreach (var sector in this._map.AllSectors)
                sector.RemoveActions(sector.PlacedActions.Where(a => !a.SurvivesQuest).ToArray());
        }
    }
}