using System.Collections.Generic;
using Artefacts.Scripts.API.Communication.Quiz;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable ClassNeverInstantiated.Global

namespace Artefacts.Scripts.API.Gameplay {
    public class Level {
        private float _publicAwareness = -1;

        public bool EventLoaded = false;

        public int Id { get; protected set; }
        public int Next { get; protected set; }
        public int EventDuration { get; protected set; }
        public int PreparationTime { get; protected set; }
        public int AdditionalMoney { get; protected set; }
        public long EventAt { get; protected set; }
        public PaConfiguration PublicAwarenessConfiguration { get; protected set; }
        public float BoostFromPrivateActions { get; protected set; }
        public float Interest { get; protected set; }
        public float FastForwardTimeFactor { get; protected set; } = 3f;
        public bool CameraIsLocked { get; protected set; }
        public bool MinimapIsClickable { get; protected set; }
        public int RainIndex { get; protected set; }
        public IEnumerable<int> AffectedSectors { get; protected set; }
        public IEnumerable<int> ActiveSectors { get; protected set; }
        public IEnumerable<float> PreventedDamageRatingThresholds { get; protected set; }
        public IEnumerable<string> EnabledActions { get; protected set; }
        public IDictionary<int, float> RainIndexDistribution { get; protected set; }
        public IDictionary<int, float> SectorImportance { get; protected set; }

        public float PublicAwareness {
            get => this._publicAwareness < 0
                       ? this.PublicAwarenessConfiguration.InitialPublicAwareness
                       : this._publicAwareness;
            private set => this._publicAwareness = value;
        }

        public Quizmaster Quizmaster { get; } = new Quizmaster();

        public void RaisePublicAwarenessBy(float raiseBy) {
            if (this.PublicAwareness + raiseBy < 1)
                this.PublicAwareness += raiseBy;
            else
                this.PublicAwareness = 1;
        }

        // Called when Button to increase Public Awareness is clicked to often in short amount in time
        // AND every few time steps.
        public void DecreasePublicAwarenessBy(float decreaseBy) {
            if (this.PublicAwareness - decreaseBy > 0)
                this.PublicAwareness -= decreaseBy;
            else
                this.PublicAwareness = 0;
        }

        public class PaConfiguration {
            public float InitialPublicAwareness { get; protected set; }
            public PaDisplay Display { get; protected set; }
            public PaButton Button { get; protected set; }
            public float QuizBonus { get; protected set; }

            public class PaDisplay {
                public float DecreaseWithTime { get; protected set; } = 0.05f;
                public int TimeStepsForDecrease { get; protected set; } = 8;
            }

            public class PaButton {
                public float OnClickRaiseBy { get; protected set; } = 0.2f;
                public float OnClickPenaltyDecreaseBy { get; protected set; } = 0.1f;
                public int PenaltyTimeSteps { get; protected set; } = 5;
            }
        }
    }
}