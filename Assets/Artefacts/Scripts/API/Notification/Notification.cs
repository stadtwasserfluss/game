using Artefacts.Scripts.API.Gameplay;

namespace Artefacts.Scripts.API.Notification {
    public class Notification : IGameTimeComponent {
        public Notification(long duration = 1) {
            GameTime.I.AddComponent(this);

            this.Duration = duration;
            this.DurationLeft = this.Duration;
        }

        public long Duration { get; }
        public long DurationLeft { get; private set; }

        public virtual void BeforeTimeStep(long now) { }

        public virtual void OnTimeStep(long now) { }

        public virtual void AfterTimeStep(long now) {
            this.DurationLeft--;
        }
    }

    public class Notification<T> : Notification {
        public Notification(T property, long duration = 1) : base(duration) {
            this.Property = property;
        }

        public T Property { get; }
    }
}