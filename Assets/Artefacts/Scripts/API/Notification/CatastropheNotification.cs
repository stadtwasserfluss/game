using Artefacts.Scripts.API.Catastrophe;

namespace Artefacts.Scripts.API.Notification {
    public class CatastropheNotification : DelayedNotification<Rain> {
        public CatastropheNotification(Rain rain)
            : base(rain.WarnAt, rain, rain.StartAt - rain.WarnAt + rain.Duration) { }

        public Rain Rain => this.Property;
    }
}