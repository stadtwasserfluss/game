namespace Artefacts.Scripts.API.Notification {
    public class DelayedNotification : Notification {
        public DelayedNotification(long startAt, long duration = 1) : base(duration) {
            this.StartAt = startAt;
        }

        public long StartAt { get; }

        public override void AfterTimeStep(long now) {
            if (now >= this.StartAt) base.AfterTimeStep(now);
        }
    }

    public class DelayedNotification<T> : DelayedNotification {
        public DelayedNotification(long startAt, T property, long duration = 1)
            : base(startAt, duration) {
            this.Property = property;
        }

        public T Property { get; }
    }
}