using Artefacts.Scripts.API.Catastrophe;

namespace Artefacts.Scripts.API.Notification {
    public class CatastrophePassedNotification : DelayedNotification<Rain> {
        public CatastrophePassedNotification(Rain rain)
            : base(rain.EndAt + 1, rain) { }

        public Rain Rain => this.Property;
    }
}