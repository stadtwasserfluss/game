﻿using System.Linq;
using Artefacts.Scripts.API.Gameplay;
using Artefacts.Scripts.API.Map;
using Artefacts.Scripts.UI.Map;
using UnityEngine;

namespace Artefacts.Scripts {
    public class GameController : MonoBehaviour {
        private GameManager _gm;
        private GameTime _gt;
        private SectorMap _map;

        public MapRepresentation MapRepresentation { get; private set; }

        private void Awake() {
            this._gt = GameTime.I;
            this._gm = GameManager.I;
        }

        private void Start() {
            this.MapRepresentation = this.GetComponentInChildren<MapRepresentation>();
            this._map = new SectorMap(from sr in this.MapRepresentation.GetAllSectors()
                                      select sr.Sector);
            this._gm.SetMap(this._map);
        }

        private void Update() {
            /* Notify GameTime about the next frame: passed time since last frame */
            this._gt.Tick((long) (Time.deltaTime * 1000));

            if (this._gm.GameState == GameManager.GameStates.Ended) {
                // End the game
            }
        }
    }
}