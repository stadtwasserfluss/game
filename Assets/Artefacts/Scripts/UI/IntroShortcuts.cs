using UnityEngine;

namespace Artefacts.Scripts.UI {
    public class IntroShortcuts : MonoBehaviour {
        public GameObject exitMenu;

        private void Update() {
            if (!Input.GetKeyDown(KeyCode.Escape)) return;

            bool isActive = this.exitMenu.activeSelf;

            this.exitMenu.SetActive(!isActive);
        }
    }
}