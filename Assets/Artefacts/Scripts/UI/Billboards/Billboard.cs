using Artefacts.Scripts.API.Gameplay;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Artefacts.Scripts.UI.Billboards {
    public class Billboard : MonoBehaviour {
        private const float STEP_HEIGHT = 12;

        // ReSharper disable once InconsistentNaming
        public string actionID;
        public TextMeshProUGUI nameText;
        public Slider slider;
        private string _actionName;
        private Canvas _canvas;
        private float _initialYposition;

        public bool IsVisible { get; private set; }

        private void Awake() {
            this._initialYposition = this.transform.position.y;
            this._canvas = this.GetComponent<Canvas>();
        }

        private void Start() {
            this._actionName = GameManager.I.AvailableActions.GetBlueprintById(this.actionID)
              ?.HumanReadableName;
            this.nameText.text = this._actionName;
        }

        private void Update() {
            this._canvas.enabled = this.IsVisible;
        }

        public void SetSliderValue(float percentage) {
            if (percentage < 0 || percentage > 1) return;
            this.slider.value = percentage * 100;
            this.nameText.text = this._actionName + $": {percentage * 100,3:##0}%";
        }

        public void Show() {
            this.IsVisible = true;
        }

        public void Hide() {
            this.IsVisible = false;
        }

        public void MoveToStep(int step) {
            this.transform.Translate(0, this._initialYposition - this.transform.position.y, 0);
            this.transform.Translate(0, Billboard.STEP_HEIGHT * step, 0);
        }
    }
}