﻿using System.Collections.Generic;
using System.Linq;
using Artefacts.Scripts.UI.Map;
using UnityEngine;

namespace Artefacts.Scripts.UI.Billboards {
    public class BillboardManager : MonoBehaviour {
        public SectorRepresentation sectorRepresentation;
        private IEnumerable<Billboard> _availableBillboards;

        // Load available action Billboards.
        private void Start() {
            this._availableBillboards = this.GetComponentsInChildren<Billboard>();
        }

        // Check whether Sector has Actions implemented and display Billboard if necessary. Update loading bars on Billboards. 
        private void Update() {
            // Show/hide billboards
            var buildingActions =
                this.sectorRepresentation.Sector.PlacedActions.Where(a => a.BuildTimeLeft > 0).ToList();
            var showing =
                this._availableBillboards.Where(b => buildingActions.Select(a => a.Id).Contains(b.actionID)).ToList();
            var hiding = this._availableBillboards.Except(showing).ToList();

            showing.ForEach(b => b.Show());
            hiding.ForEach(b => b.Hide());

            // Set their position respective to the number of active Billboards
            for (var i = 0; i < showing.Count; i++) showing[i].MoveToStep(i);

            // Update sliders on active Billboards
            foreach (var billboard in showing) {
                var action = buildingActions.First(a => a.Id == billboard.actionID);
                billboard.SetSliderValue(1f - (float) action.BuildTimeLeft / action.BuildTime);
            }
        }
    }
}