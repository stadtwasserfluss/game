using UnityEngine;

namespace Artefacts.Scripts.UI.Map {
    /**
     * Represents the highlighting border of each sector that lights up whenever a sector is selected. Sits on the
     * sector selection walls Gameobjects.
     */
    public class SectorHighlighting : MonoBehaviour {
        public SectorRepresentation sectorRepresentation;
        private MeshRenderer _lineRenderer;

        private void Awake() {
            this._lineRenderer = this.GetComponent<MeshRenderer>();
        }

        private void Update() {
            this._lineRenderer.enabled = this.sectorRepresentation.IsSelected;
        }
    }
}