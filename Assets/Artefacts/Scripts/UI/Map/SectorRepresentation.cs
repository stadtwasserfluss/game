using System.Linq;
using Artefacts.Scripts.API.Gameplay;
using Artefacts.Scripts.API.Map;
using UnityEngine;

namespace Artefacts.Scripts.UI.Map {
    /**
     * The UI representation of a Sector. Sits on the Sector Gameobjects.
     */
    public class SectorRepresentation : MonoBehaviour {
        public int id;
        public AudioSource selectSound;
        [SerializeField] public ActionRepresentation[] actionPrefabs;

        private MapRepresentation _mapRepresentation;

        public Sector Sector { get; private set; }
        public bool IsSelected { get; private set; }

        private void Awake() {
            this.Sector = SectorConfigurationList.Instance.GetById(this.id) ?? new Sector();
        }

        private void Start() {
            this.GetComponentInChildren<SectorClickListener>().AddClickHandler(this.OnSectorClick);

            foreach (var actionRepresentation in this.actionPrefabs) actionRepresentation.Instantiate();
        }

        private void Update() {
            var activeActions =
                (from a in this.Sector.PlacedActions
                 where a.IsEffective
                 group a by a.Id
                 into ag
                 select new {Id = ag.Key, Count = ag.Count()}).ToList();

            var inactiveActions = this.Sector.AvailableActions.Except(from a in activeActions select a.Id);

            foreach (var action in activeActions)
                this.actionPrefabs.FirstOrDefault(a => a.actionId == action.Id)?.Show(action.Count);

            foreach (string action in inactiveActions)
                this.actionPrefabs.FirstOrDefault(a => a.actionId == action)?.Show(0);
        }

        public void Select(bool single = true) {
            if (single) this._mapRepresentation.DeselectAllSectors();

            this.IsSelected = true;
            GameManager.I.CurrentStory?.Notify($"selected-sector-{this.id}");
        }

        public void Deselect() {
            this.IsSelected = false;
            GameManager.I.CurrentStory?.Notify($"deselected-sector-{this.id}");
        }

        public void SetMap(MapRepresentation map) {
            this._mapRepresentation = map;
        }

        private void OnSectorClick() {
            if (!this.Sector.IsActive)
                return;

            this.selectSound.Play();

            if (this.IsSelected)
                this.Deselect();
            else
                this.Select();
        }
    }
}