using Artefacts.Scripts.API.Gameplay;
using UnityEngine;

namespace Artefacts.Scripts.UI.Map {
    /**
     * Represents the Sector in the minimap. Is used to switch colors.
     */
    public class SectorMinimapRepresentation : MonoBehaviour {
        private static readonly Color SelectionColor = Color.green;
        private static readonly Color AffectedColor = Color.yellow;
        private static readonly Color DeactivatedColor = Color.gray;

        public SectorRepresentation sectorRepresentation;

        private EventManager _em;
        private Color _initialColor;
        private Renderer _minimapRenderer;
        private Status _status;

        private Color Color {
            get => this._minimapRenderer.material.color;
            set => this._minimapRenderer.material.color = value;
        }

        private void Awake() {
            this._minimapRenderer = this.GetComponent<Renderer>();
            this._initialColor = this.Color;
            this._status = Status.Unmarked;
            this._em = EventManager.I;
        }

        private void Update() {
            var previousStatus = this._status;
            this._status = Status.Unmarked;

            if (!this.sectorRepresentation.Sector.IsActive) this._status = Status.Disabled;

            if (this._em.IsSectorAffectedByNextCatastrophe(this.sectorRepresentation.Sector)
                && this._status != Status.Affected)
                this._status = Status.Affected;

            if (this.sectorRepresentation.IsSelected) this._status = Status.Selected;

            if (this._status == previousStatus) return;

            this.Unmark();

            switch (this._status) {
                case Status.Disabled:
                    this.MarkAsDisabled();
                    break;
                case Status.Affected:
                    this.MarkAsAffected();
                    break;
                case Status.Selected:
                    this.MarkAsSelected();
                    break;
                case Status.Unmarked:
                    this.Unmark();
                    break;
                default:
                    this.Unmark();
                    break;
            }
        }

        private void Unmark() {
            this.Color = this._initialColor;
        }

        private void MarkAsSelected() {
            this.Color = SectorMinimapRepresentation.SelectionColor;
        }

        private void MarkAsAffected() {
            this.Color = SectorMinimapRepresentation.AffectedColor;
        }

        private void MarkAsDisabled() {
            this.Color = SectorMinimapRepresentation.DeactivatedColor;
        }

        private enum Status {
            Unmarked = 0, Selected = 1, Affected = 1 << 1, Disabled = 1 << 2
        }
    }
}