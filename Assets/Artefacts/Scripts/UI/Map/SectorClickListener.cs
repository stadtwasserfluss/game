using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Artefacts.Scripts.UI.Map {
    /**
     * Sits on each Sector Mesh. This listens for clicks on the sector and executes the added functions on click.
     */
    public class SectorClickListener : MonoBehaviour {
        public delegate void ClickHandler();

        private List<ClickHandler> _handlers;

        private void Awake() {
            this._handlers = new List<ClickHandler>();
        }

        private void OnMouseUp() {
            if (!EventSystem.current.IsPointerOverGameObject())
                this._handlers.ForEach(h => h.Invoke());
        }

        public void AddClickHandler(ClickHandler handler) {
            this._handlers.Add(handler);
        }
    }
}