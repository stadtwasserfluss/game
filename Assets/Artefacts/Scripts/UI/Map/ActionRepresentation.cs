using System;
using UnityEngine;

namespace Artefacts.Scripts.UI.Map {
    [Serializable]
    public class ActionRepresentation {
        public string actionId;
        public GameObject[] prefabs;

        private GameObject[] _instances;

        public void Instantiate() {
            this._instances = new GameObject[this.prefabs?.Length ?? 0];

            for (var i = 0; i < (this.prefabs?.Length ?? 0); i++) {
                var prefab = this.prefabs?[i];

                if (prefab != null) {
                    var instance = UnityEngine.Object.Instantiate(prefab);
                    instance.SetActive(false);
                    this._instances[i] = instance;
                } else {
                    this._instances[i] = null;
                }
            }
        }

        public void Show(int stack) {
            for (var i = 0; i <= stack && i < this._instances.Length; i++) {
                var instance = this._instances[i];

                if (instance == null)
                    continue;

                instance.SetActive(true);
            }

            for (int i = stack + 1; i < this._instances.Length; i++) {
                var instance = this._instances[i];

                if (instance == null)
                    continue;

                instance.SetActive(false);
            }
        }
    }
}