using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Artefacts.Scripts.UI.Map {
    /**
     * Sits on the "Map" Gameobject. It provides UI functions for all sectors.
     */
    public class MapRepresentation : MonoBehaviour {
        private IEnumerable<SectorRepresentation> _sectorRepresentations;

        public bool HasOneSelectedSector =>
            this._sectorRepresentations.Count(sr => sr.IsSelected) == 1;

        public bool HasMultipleSelectedSectors =>
            this._sectorRepresentations.Count(sr => sr.IsSelected) > 1;

        public SectorRepresentation SelectedSector =>
            this._sectorRepresentations.FirstOrDefault(sr => sr.IsSelected);

        private void Awake() {
            this._sectorRepresentations = from sr in this.GetComponentsInChildren<SectorRepresentation>()
                                          where sr.Sector != null
                                          select sr;
        }

        private void Start() {
            foreach (var sr in this._sectorRepresentations) sr.SetMap(this);
        }

        public IEnumerable<SectorRepresentation> GetAllSectors() {
            return this._sectorRepresentations;
        }

        public void DeselectAllSectors() {
            foreach (var sr in this._sectorRepresentations) sr.Deselect();
        }

        public SectorRepresentation SectorById(int id) =>
            this._sectorRepresentations.FirstOrDefault(sr => sr.id == id);
    }
}