using System.Collections.Generic;
using Artefacts.Scripts.API.Communication;
using Artefacts.Scripts.API.Gameplay;
using UnityEngine;
using Button = UnityEngine.UI.Button;

namespace Artefacts.Scripts.UI.Communications {
    public class CommunicationsWindow : MonoBehaviour {
        public Button[] directPageLinks;
        public Button hideWindow;
        public Button nextPage;
        public GameObject[] pages;
        public Button previousPage;
        public Button showWindow;
        public int startPage;
        public AudioSource emailSound;
        public AudioSource resultsSound;

        private readonly List<Email> _receivedEmails = new List<Email>();

        private int _currentEmailNum;
        private int _currentPage;
        private bool _gamePaused;
        private bool _gameStart;
        private GameManager _gm;
        private GameTime _gt;
        private Canvas _self;

        public Email CurrentEmail { get; private set; }

        private void Start() {
            this._gm = GameManager.I;
            this._currentPage = this.startPage;
            this._self = this.GetComponent<Canvas>();

            foreach (var page in this.pages) page.SetActive(false);

            this.pages[this._currentPage].SetActive(true);

            for (var i = 0; i < this.directPageLinks.Length; i++) {
                int j = i;
                this.directPageLinks[i].onClick.AddListener(() => this.ShowPage(j));
            }

            this.previousPage.onClick.AddListener(this.ShowPreviousPage);
            this.nextPage.onClick.AddListener(this.ShowNextPage);

            this._gm = GameManager.I;
            this._gt = GameTime.I;
            this._gameStart = true;
        }

        private void Update() {
            var email = this._gm.CurrentStory?.NextEmail;

            if (email is null) return;

            // New Email received
            this._receivedEmails.Add(email);
            this.emailSound.Play();
            this.SelectEmail(this._receivedEmails.Count - 1);
            this._gm.CurrentStory?.Notify($"received-email-{email.Id}");
            this.Show(0);
        }

        public void Show() {
            this._self.enabled = true;
            this.showWindow.gameObject.SetActive(false);
            this.hideWindow.gameObject.SetActive(true);
            this.ShowPage(this._currentPage);
            this._gamePaused = GameTime.I.IsPaused;
            if (!this._gamePaused) GameTime.I.Pause();
        }

        public void Show(int pageId) {
            this.Show();
            this.ShowPage(pageId);
        }

        public void Hide() {
            this._self.enabled = false;
            this.showWindow.gameObject.SetActive(true);
            this.hideWindow.gameObject.SetActive(false);
            this.HidePage(this._currentPage);
            if (!this._gamePaused || this._gameStart) {
                if (this._gameStart) this._gameStart = false;

                GameTime.I.Unpause();
            }

            this._gm.CurrentStory?.Notify($"closed-email-{this.CurrentEmail.Id}");
        }

        private void ShowNextPage() {
            if (this._currentPage == 0 && this._currentEmailNum < this._receivedEmails.Count - 1) {
                this._currentEmailNum++;
                this.CurrentEmail.Read = true;
                this.SelectEmail(this._currentEmailNum);
            } else {
                int nextId = (this._currentPage + 1) % this.pages.Length;
                if (nextId == 1 &&
                    !(this._gt.Now >= this._gm.CurrentLevel.EventAt + this._gm.CurrentLevel.EventDuration))
                    nextId = (this._currentPage + 2) % this.pages.Length;
                if (nextId == 0) {
                    this._currentEmailNum = 0;
                    this.CurrentEmail.Read = true;
                    this.SelectEmail(this._currentEmailNum);
                }

                this.ShowPage(nextId);
            }
        }

        private void ShowPreviousPage() {
            if (this._currentEmailNum > 0) {
                this._currentEmailNum--;
                this.CurrentEmail.Read = true;
                this.SelectEmail(this._currentEmailNum);
            } else {
                int prevId = (this._currentPage + this.pages.Length - 1) % this.pages.Length;
                if (prevId == 1 &&
                    !(this._gt.Now >= this._gm.CurrentLevel.EventAt + this._gm.CurrentLevel.EventDuration))
                    prevId = (this._currentPage + this.pages.Length - 2) % this.pages.Length;
                if (prevId == 0) {
                    if (this._receivedEmails.Count == 1)
                        this._currentEmailNum = 0;
                    else
                        this._currentEmailNum = this._receivedEmails.Count - 1;
                    this.CurrentEmail.Read = true;
                    this.SelectEmail(this._currentEmailNum);
                }

                this.ShowPage(prevId);
            }
        }

        private void ShowPage(int pageId) {
            if (pageId != this._currentPage) {
                this.HidePage(this._currentPage);

                if (pageId == 1) this.resultsSound.Play();
            }

            this.pages[pageId].SetActive(true);
            this._currentPage = pageId;
        }

        private void HidePage(int pageId) {
            this.pages[pageId].SetActive(false);

            if (pageId == 0) this.CurrentEmail.Read = true;
        }

        private void SelectEmail(int num) {
            this.CurrentEmail = this._receivedEmails[num];
        }
    }
}