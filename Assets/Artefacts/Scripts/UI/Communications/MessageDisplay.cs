﻿using Artefacts.Scripts.API.Gameplay;
using UnityEngine;

namespace Artefacts.Scripts.UI.Communications {
    public class MessageDisplay : MonoBehaviour {
        public int smallWidth = 105;
        public int smallHeight = 150;
        public int largeWidth = 175;
        public int largeHeight = 250;
        public AudioSource messageSound;

        private Vector2 _anchorMaxLarge;
        private Vector2 _anchorMaxSmall;
        private Vector2 _anchorMinLarge;
        private Vector2 _anchorMinSmall;

        private GameManager _gm;
        private bool _large;
        private MessageContainer _messageContainer;
        private Vector2 _pivotLarge;
        private Vector2 _pivotSmall;
        private Vector2 _positionLarge;
        private Vector2 _positionSmall;
        private RectTransform _rectTransform;
        private Vector3 _scaleLarge;
        private Vector3 _scaleSmall;

        private void Start() {
            this._gm = GameManager.I;
            this._messageContainer = this.GetComponentInChildren<MessageContainer>();
            this._rectTransform = this.GetComponent<RectTransform>();
            this._positionSmall = this._rectTransform.anchoredPosition;

            this._positionLarge = new Vector2(0, -11.6f);
            this._scaleSmall = new Vector3(1, 1, 1);
            this._scaleLarge = new Vector3(1.5f, 1.5f, 1.5f);

            this._pivotSmall = new Vector2(0, 1);
            this._pivotLarge = new Vector2(0.5f, 1f);

            this._anchorMinSmall = new Vector2(0, 1);
            this._anchorMaxSmall = new Vector2(0, 1);
            this._anchorMinLarge = new Vector2(0.5f, 1f);
            this._anchorMaxLarge = new Vector2(0.5f, 1f);
        }

        private void Update() {
            var message = this._gm.CurrentStory?.NextMessage;
            if (message is null) return;

            this._messageContainer.AddMessage(message);
            this.messageSound.Play();
        }

        public void ToggleSize() {
            if (this._large) {
                this.Reduce();
                this._large = false;
            } else {
                this.Enlarge();
                this._large = true;
            }

            this._messageContainer.ForceLayoutUpdate();
        }

        private void Enlarge() {
            this._rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, this.largeWidth);
            this._rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, this.largeHeight);
            this._rectTransform.anchoredPosition = this._positionLarge;
            this._rectTransform.localScale = this._scaleLarge;
            this._rectTransform.pivot = this._pivotLarge;
            this._rectTransform.anchorMin = this._anchorMinLarge;
            this._rectTransform.anchorMax = this._anchorMaxLarge;
        }

        private void Reduce() {
            this._rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, this.smallWidth);
            this._rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, this.smallHeight);
            this._rectTransform.anchoredPosition = this._positionSmall;
            this._rectTransform.localScale = this._scaleSmall;
            this._rectTransform.anchorMin = this._anchorMinSmall;
            this._rectTransform.anchorMax = this._anchorMaxSmall;
            this._rectTransform.pivot = this._pivotSmall;
        }
    }
}