﻿using System.Linq;
using Artefacts.Scripts.API.Gameplay;
using Artefacts.Scripts.API.Notification;
using UnityEngine;
using Button = UnityEngine.UI.Button;

namespace Artefacts.Scripts.UI.Communications {
    public class ButtonStatistics : MonoBehaviour {
        public Button statisticsButton;
        private GameManager _gm;
        private GameTime _gt;

        private void Start() {
            this._gt = GameTime.I;
            this._gm = GameManager.I;
            this.statisticsButton.interactable = false;
        }

        private void Update() {
            this.statisticsButton.interactable =
                EventManager.I.Notifications.OfType<CatastrophePassedNotification>().Any();
            // this.statisticsButton.interactable = this._gt.Now >= this._gm.CurrentLevel.EventAt;
        }
    }
}