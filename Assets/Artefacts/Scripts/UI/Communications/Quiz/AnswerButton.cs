using Artefacts.Scripts.API.Communication.Quiz;
using Artefacts.Scripts.API.Gameplay;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;

namespace Artefacts.Scripts.UI.Communications.Quiz {
    public class AnswerButton : MonoBehaviour {
        private static ColorBlock _wrongColors;
        private static ColorBlock _correctColors;
        private static ColorBlock _normalColors;

        [HideInInspector] public AudioSource answerCorrect;
        [HideInInspector] public AudioSource answerIncorrect;
        private Button _button;
        private bool _correct;
        private TextMeshProUGUI _text;

        public Question.Answer Answer { get; set; }

        private void Start() {
            this._button = this.GetComponent<Button>();

            AnswerButton._wrongColors = AnswerButton._correctColors = AnswerButton._normalColors = this._button.colors;
            AnswerButton._wrongColors.disabledColor = new Color(179 / 255f, 27 / 255f, 27 / 255f);
            AnswerButton._correctColors.disabledColor = new Color(13 / 255f, 178 / 255f, 15 / 255f);

            this._button.onClick.AddListener(() => {
                this._correct = GameManager.I.CurrentLevel.Quizmaster.SetAnswer(this.Answer);

                if (this._correct) {
                    this._button.colors = AnswerButton._correctColors;
                    this.answerCorrect.Play();
                } else {
                    this._button.colors = AnswerButton._wrongColors;
                    this.answerIncorrect.Play();
                }
            });

            this._text = this.GetComponentInChildren<TextMeshProUGUI>();
        }

        private void Update() {
            if (this.Answer is null)
                return;

            this._text.text = this.Answer.Text;
        }

        public void Disable() {
            this._button.interactable = false;
        }

        public void Enable() {
            this._button.interactable = true;
            this._button.colors = AnswerButton._normalColors;
        }
    }
}