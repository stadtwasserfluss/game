﻿using Artefacts.Scripts.API.Gameplay;
using TMPro;
using UnityEngine;
using Button = UnityEngine.UI.Button;

namespace Artefacts.Scripts.UI.Communications.Quiz {
    public class QuestionnaireWindow : MonoBehaviour {
        public Canvas quizWindow;
        public Button nextButton;
        public TextMeshProUGUI question;
        public AnswerButton[] answerButtons;
        public AudioSource answerCorrect;
        public AudioSource answerIncorrect;

        private GameManager _gm;

        private void Start() {
            this._gm = GameManager.I;

            this.nextButton.onClick.AddListener(() => {
                this._gm.CurrentLevel.Quizmaster.NextQuestion();

                if (!(this._gm.CurrentLevel.Quizmaster.CurrentQuestion is null))
                    return;

                GameTime.I.Unpause();
                this.Disable();
            });

            foreach (var ab in this.answerButtons) {
                ab.answerCorrect = this.answerCorrect;
                ab.answerIncorrect = this.answerIncorrect;
            }
        }

        private void Update() {
            var currentQuestion = this._gm.CurrentLevel.Quizmaster.CurrentQuestion;

            if (currentQuestion is null)
                return;


            this.Enable();
            GameTime.I.Pause();
            this.question.text = currentQuestion.Text;

            for (var i = 0; i < this.answerButtons.Length; i++)
                this.answerButtons[i].Answer = currentQuestion.Answers[i];

            foreach (var button in this.answerButtons)
                if (currentQuestion.IsAnswered)
                    button.Disable();
                else
                    button.Enable();
        }

        private void Enable() {
            this.quizWindow.enabled = true;
            this.quizWindow.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }

        private void Disable() {
            this.quizWindow.enabled = false;
            this.quizWindow.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }
}