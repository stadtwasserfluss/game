using System.Linq;
using Artefacts.Scripts.API.Gameplay;
using Artefacts.Scripts.API.Map;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Artefacts.Scripts.UI.Communications {
    public class EventResults : MonoBehaviour {
        public Slider damagePreventedSlider;
        public TextMeshProUGUI headlineText;
        public Slider publicAwarenessSlider;
        public TextMeshProUGUI questionaireScoreText;
        public Image[] stars;
        private readonly Color _starDisabledColor = new Color(140f / 255f, 140f / 255f, 140f / 255f);
        private GameManager _gm;
        private string _headlineFormat;
        private SectorMap _map;
        private Color _starEnabledColor;

        private void Start() {
            this._gm = GameManager.I;
            this._map = this._gm.Map;
            this._headlineFormat = this.headlineText.text;
            this._starEnabledColor = this.stars[0].color;

            this.HighlightStars(0);
        }

        private void Update() {
            var currentLevel = this._gm.CurrentLevel;

            this.headlineText.text = string.Format(this._headlineFormat, currentLevel.Id);

            var qm = this._gm.CurrentLevel.Quizmaster;
            this.questionaireScoreText.text = $"{qm.CorrectAnswers}/{qm.TotalAnswers}";

            float prevented = (float) this._map.TotalWeightedProtection / this._map.TotalDamage;
            float publicBonus = currentLevel.BoostFromPrivateActions * currentLevel.PublicAwareness;

            this.damagePreventedSlider.value = prevented;
            this.publicAwarenessSlider.value = publicBonus;

            float total = prevented + publicBonus;
            var ratingThresholds = currentLevel.PreventedDamageRatingThresholds.ToArray();

            this.HighlightStars(ratingThresholds.Count(threshold => total >= threshold));
        }

        private void HighlightStars(int count) {
            for (var i = 0; i < this.stars.Length; i++)
                this.stars[i].color = i < count ? this._starEnabledColor : this._starDisabledColor;
        }
    }
}