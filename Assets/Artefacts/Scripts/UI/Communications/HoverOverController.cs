using System;
using Artefacts.Scripts.API.Map;
using UnityEngine;

namespace Artefacts.Scripts.UI.Communications {
    public class HoverOverController : MonoBehaviour {
        public GameObject hoverOverNotEnoughMoney;
        public GameObject hoverOverNotAllowedByLevel;
        public GameObject hoverOverNotAllowedBySector;
        public GameObject hoverOverNotStackable;
        public GameObject hoverOverStackLimitReached;
        public GameObject hoverOverRequirementNotImplemented;
        public GameObject hoverOverStackingDelayed;
        public GameObject hoverOverNoAreaLeft;
        public GameObject hoverOverWrongGamePhase;

        public void ShowHoverOverForResult(Sector.ActionPlacementResult apr) {
            this.SelectHoverOverForResult(apr)?.SetActive(true);
        }

        public void HideHoverOvers(Sector.ActionPlacementResult apr) {
            this.SelectHoverOverForResult(apr)?.SetActive(false);
        }

        private GameObject SelectHoverOverForResult(Sector.ActionPlacementResult apr) {
            switch (apr) {
                case Sector.ActionPlacementResult.Ok:
                    return null;
                case Sector.ActionPlacementResult.NotAllowedByLevel:
                    return this.hoverOverNotAllowedByLevel;
                case Sector.ActionPlacementResult.RequirementNotImplemented:
                    return this.hoverOverRequirementNotImplemented;
                case Sector.ActionPlacementResult.NotStackable:
                    return this.hoverOverNotStackable;
                case Sector.ActionPlacementResult.BlueprintNotAllowed:
                    return null;
                case Sector.ActionPlacementResult.StackingDelayed:
                    return this.hoverOverStackingDelayed;
                case Sector.ActionPlacementResult.StackLimitReached:
                    return this.hoverOverStackLimitReached;
                case Sector.ActionPlacementResult.NoAreaLeft:
                    return this.hoverOverNoAreaLeft;
                case Sector.ActionPlacementResult.NotAllowedBySector:
                    return this.hoverOverNotAllowedBySector;
                case Sector.ActionPlacementResult.NotEnoughMoney:
                    return this.hoverOverNotEnoughMoney;
                case Sector.ActionPlacementResult.WrongGamePhase:
                    return this.hoverOverWrongGamePhase;
                default:
                    throw new ArgumentOutOfRangeException(nameof(apr), apr, null);
            }
        }
    }
}