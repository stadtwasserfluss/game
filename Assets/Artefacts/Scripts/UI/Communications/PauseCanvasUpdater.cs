﻿using Artefacts.Scripts.API.Gameplay;
using UnityEngine;

namespace Artefacts.Scripts.UI.Communications {
    public class PauseCanvasUpdater : MonoBehaviour {
        public GameObject PauseCanvas;

        public GameObject MailCanvas;
        public GameObject StatisticsCanvas;

        private void Update() {
            if (GameTime.I.IsPaused) {
                if ((this.MailCanvas.activeSelf || this.StatisticsCanvas.activeSelf) && this.PauseCanvas.activeSelf)
                    this.PauseCanvas.SetActive(false);
                else if (!this.MailCanvas.activeSelf && !this.StatisticsCanvas.activeSelf &&
                         !this.PauseCanvas.activeSelf)
                    this.PauseCanvas.SetActive(true);
            } else {
                if ((this.MailCanvas.activeSelf || this.StatisticsCanvas.activeSelf) && !GameTime.I.IsPaused)
                    GameTime.I.Pause();
            }
        }
    }
}