﻿using Artefacts.Scripts.API.Gameplay;
using UnityEngine;

namespace Artefacts.Scripts.UI.Communications {
    public class SectorResultsController : MonoBehaviour {
        public GameObject SectorResultPrefab;
        private int _currentLevel;

        private GameManager _gm;

        private void Start() {
            this._gm = GameManager.I;
            this._currentLevel = this._gm.CurrentLevel.Id;

            this.CreateSectorResults();
        }

        public void Update() {
            if (this._currentLevel != this._gm.CurrentLevel.Id) {
                this._currentLevel = this._gm.CurrentLevel.Id;
                this.DeleteSectorResults();
                this.CreateSectorResults();
            }
        }

        // Instantiate Sector Results Object for each Sector in Level
        private void CreateSectorResults() {
            foreach (int sector in this._gm.CurrentLevel.ActiveSectors) {
                var newSectorCanvas = Object.Instantiate(this.SectorResultPrefab, this.transform);
                var newSectorValues = newSectorCanvas.GetComponent<SectorStatObject>();
                newSectorValues.SectorID = sector;
            }
        }


        // Destroy Sector Results Objects 
        private void DeleteSectorResults() {
            for (int i = this.transform.childCount - 1; i >= 0; i--) this.transform.GetChild(i).SetParent(null);
        }
    }
}