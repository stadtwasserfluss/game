using Artefacts.Scripts.API.Communication;
using UnityEngine;
using UnityEngine.UI;

namespace Artefacts.Scripts.UI.Communications {
    public class MessageContainer : MonoBehaviour {
        public MessageRepresentation messageBodyPrefab;

        private RectTransform _rectTransform;

        private void Start() {
            // Remove all messages that could be left
            for (int i = this.transform.childCount - 1; i >= 0; i--)
                this.transform.GetChild(i).SetParent(null);

            this._rectTransform = this.GetComponent<RectTransform>();
        }

        public void AddMessage(Message message) {
            var instance = Object.Instantiate(this.messageBodyPrefab, this.transform, false);
            instance.transform.SetAsLastSibling();
            instance.Message = message;

            if (this.transform.childCount == 1) this.ForceLayoutUpdate();
        }

        public void ForceLayoutUpdate() {
            LayoutRebuilder.ForceRebuildLayoutImmediate(this._rectTransform);
        }
    }
}