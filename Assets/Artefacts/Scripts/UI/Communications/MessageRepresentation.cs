using Artefacts.Scripts.API.Communication;
using TMPro;
using UnityEngine;

namespace Artefacts.Scripts.UI.Communications {
    public class MessageRepresentation : MonoBehaviour {
        public TextMeshProUGUI messageText;
        public TextMeshProUGUI senderText;

        public Message Message {
            set {
                this.senderText.text = value.From;
                this.messageText.text = value.Text;
            }
        }
    }
}