﻿using Artefacts.Scripts.API.Gameplay;
using Artefacts.Scripts.API.Map;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Artefacts.Scripts.UI.Communications {
    public class SectorStatObject : MonoBehaviour {
        public TextMeshProUGUI SectorNumber;
        public TextMeshProUGUI Damage;
        public TextMeshProUGUI Importance;
        public TextMeshProUGUI Area;
        public TextMeshProUGUI RoofArea;
        public TextMeshProUGUI GroundArea;
        public TextMeshProUGUI TotalProtection;

        // Icons of the Hover Over
        public GameObject GreenRoof;
        public GameObject UnsealSurfaces;
        public GameObject MultipurposeAreas;
        public GameObject StormwaterPlanters;
        public GameObject Sewers;
        public GameObject StormWater2;
        public GameObject CloseParking;
        public GameObject CloseStreet;

        public GameObject Sandbags;

        // Color of Icons
        private Color32 _actionImplemented;
        private bool _closeParking;
        private bool _closeStreet;
        private GameManager _gm;

        // Bool Variables, to reduce Use of GetComponent<Image>()
        private bool _greenRoof;
        private bool _multipurposeAreas;
        private bool _sandbags;

        private Sector _sector;
        private bool _sewers;
        private bool _stormwater;
        private bool _unsealSurfaces;

        public int SectorID { get; set; }

        private void Start() {
            this._gm = GameManager.I;
            this._sector = this._gm.Map.GetSectorById(this.SectorID);

            this.SectorNumber.text = this.SectorID.ToString();
            this.Importance.text = this._sector.Importance.ToString();
            this.Area.text = this._sector.AreaSector.ToString();
            this.RoofArea.text = this._sector.AvailableRoofArea.ToString();
            this.GroundArea.text = this._sector.AvailableGroundArea.ToString();

            this._actionImplemented = new Color32(138, 215, 255, 255);
        }

        // Updates values
        private void Update() {
            this.Damage.text = this._sector.Damage.ToString();
            this.TotalProtection.text = this._sector.TotalProtection.ToString();

            // Activate Icons if action has been implemented
            foreach (var action in this._sector.PlacedActions)
                switch (action.Id) {
                    case "cs" when !this._closeStreet:
                        this.CloseStreet.GetComponent<Image>().color = this._actionImplemented;
                        this._closeStreet = true;
                        break;
                    case "g" when !this._greenRoof:
                        this.GreenRoof.GetComponent<Image>().color = this._actionImplemented;
                        this._greenRoof = true;
                        break;
                    case "m" when !this._multipurposeAreas:
                        this.MultipurposeAreas.GetComponent<Image>().color = this._actionImplemented;
                        this._multipurposeAreas = true;
                        break;
                    case "sb" when !this._sandbags:
                        this.Sandbags.GetComponent<Image>().color = this._actionImplemented;
                        this._sandbags = true;
                        break;
                    case "s" when !this._sewers:
                        this.Sewers.GetComponent<Image>().color = this._actionImplemented;
                        this._sewers = true;
                        break;
                    case "p" when !this._stormwater:
                        this.StormwaterPlanters.GetComponent<Image>().color = this._actionImplemented;
                        this.StormWater2.GetComponent<Image>().color = this._actionImplemented;
                        this._stormwater = true;
                        break;
                    case "pg" when !this._closeParking:
                        this.CloseParking.GetComponent<Image>().color = this._actionImplemented;
                        this._closeParking = true;
                        break;
                    case "u" when !this._unsealSurfaces:
                        this.UnsealSurfaces.GetComponent<Image>().color = this._actionImplemented;
                        this._unsealSurfaces = true;
                        break;
                }
        }
    }
}