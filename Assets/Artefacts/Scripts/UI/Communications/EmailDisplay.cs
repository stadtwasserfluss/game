﻿using TMPro;
using UnityEngine;

namespace Artefacts.Scripts.UI.Communications {
    public class EmailDisplay : MonoBehaviour {
        public TextMeshProUGUI fromText;

        public TextMeshProUGUI messageBodyText;
        public TextMeshProUGUI subjectText;
        private CommunicationsWindow _parent;

        private void Start() {
            this._parent = this.GetComponentInParent<CommunicationsWindow>();
        }

        private void Update() {
            var email = this._parent.CurrentEmail;

            if (email is null)
                return;

            this.fromText.text = $"{email.From} <{email.FromAdress}>";
            this.subjectText.text = email.Subject;
            this.messageBodyText.text = email.Text;
        }
    }
}