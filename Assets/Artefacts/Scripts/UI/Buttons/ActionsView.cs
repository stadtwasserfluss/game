﻿using UnityEngine;

namespace Artefacts.Scripts.UI.Buttons {
    public class ActionsView : MonoBehaviour {
        public UiController uiController;
        private Canvas _canvas;

        private void Awake() {
            this._canvas = this.GetComponent<Canvas>();
            this.CloseActions();
        }

        private void Update() {
            if (this.uiController.gameController.MapRepresentation.HasOneSelectedSector)
                this.OpenActions();
            else
                this.CloseActions();
        }

        private void OpenActions() {
            this._canvas.enabled = true;
        }

        private void CloseActions() {
            this._canvas.enabled = false;
        }
    }
}