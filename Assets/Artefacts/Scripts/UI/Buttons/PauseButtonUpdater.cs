﻿using Artefacts.Scripts.API.Gameplay;
using UnityEngine;
using Button = UnityEngine.UI.Button;

namespace Artefacts.Scripts.UI.Buttons {
    public class PauseButtonUpdater : MonoBehaviour {
        public Button pauseButton;
        public Button playButton;
        public GameObject canvasPause;
        public Canvas canvasCommunications;

        private GameTime _gt;

        private void Start() {
            this._gt = GameTime.I;
        }

        private void Update() {
            if (this._gt.IsPaused && !this.playButton.IsActive()) {
                this.playButton.gameObject.SetActive(true);
                this.pauseButton.gameObject.SetActive(false);
            } else if (!this._gt.IsPaused && this.playButton.IsActive()) {
                this.playButton.gameObject.SetActive(false);
                this.pauseButton.gameObject.SetActive(true);
            }

            if (!this._gt.IsPaused && this.canvasCommunications.enabled) this._gt.Pause();
        }

        public void PauseButtonClick() {
            this._gt.Pause();
            this.canvasPause.SetActive(true);
        }

        public void PlayButtonClick() {
            this._gt.Unpause();
            this.canvasPause.SetActive(false);
        }
    }
}