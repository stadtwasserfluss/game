using Artefacts.Scripts.API.Gameplay;
using UnityEngine;
using Button = UnityEngine.UI.Button;

namespace Artefacts.Scripts.UI.Buttons {
    public class SpeedupButtonUpdater : MonoBehaviour {
        public Button activateSpeedup;
        public Button deactivateSpeedup;

        private void Update() {
            if (GameTime.I.IsSpedUp) {
                this.deactivateSpeedup.gameObject.SetActive(true);
                this.activateSpeedup.gameObject.SetActive(false);
            } else {
                this.deactivateSpeedup.gameObject.SetActive(false);
                this.activateSpeedup.gameObject.SetActive(true);
            }
        }
    }
}