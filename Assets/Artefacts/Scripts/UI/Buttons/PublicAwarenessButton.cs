﻿using Artefacts.Scripts.API.Gameplay;
using UnityEngine;

namespace Artefacts.Scripts.UI.Buttons {
    public class PublicAwarenessButton : MonoBehaviour, IGameTimeComponent {
        public GameObject MailCanvas;
        public GameObject StatisticsCanvas;
        private GameManager _gm;
        private GameTime _gt;
        private int _timeStepCounter;

        // Set References
        private void Start() {
            this._gm = GameManager.I;
            this._gt = GameTime.I;
            this._gt.AddComponent(this);
        }

        public void BeforeTimeStep(long now) { }

        public void OnTimeStep(long now) { }

        // Count down time steps, until penalty for Button clicking stops
        public void AfterTimeStep(long now) {
            if (this._timeStepCounter != 0) this._timeStepCounter--;
        }

        // Behaviour, when button is clicked: Raises PA if clicked. When clicked multiple times in short amount of time,
        // PA decreases as a penalty for spamming the button. 
        public void RaisePublicAwareness() {
            if (this.MailCanvas.activeSelf || this.StatisticsCanvas.activeSelf) {
                return;
            }

            float raiseBy = this._gm.CurrentLevel.PublicAwarenessConfiguration.Button.OnClickRaiseBy;
            float decreaseBy = this._gm.CurrentLevel.PublicAwarenessConfiguration.Button.OnClickPenaltyDecreaseBy;

            if (this._timeStepCounter == 0) {
                this._gm.CurrentLevel.RaisePublicAwarenessBy(raiseBy);
                this._timeStepCounter = this._gm.CurrentLevel.PublicAwarenessConfiguration.Button.PenaltyTimeSteps;
            } else {
                this._gm.CurrentLevel.DecreasePublicAwarenessBy(decreaseBy);
            }
        }
    }
}