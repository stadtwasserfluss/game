using System;
using System.Collections.Generic;
using Artefacts.Scripts.API.Gameplay;
using Artefacts.Scripts.API.Map;
using Artefacts.Scripts.UI.Communications;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Action = Artefacts.Scripts.API.Action.Action;
using Button = UnityEngine.UI.Button;

namespace Artefacts.Scripts.UI.Buttons {
    public class ActionDeployment : MonoBehaviour {
        public UiController uiController;
        public GameObject actionDeploymentCanvas;
        public string actionId;
        public TextMeshProUGUI buildTimeText;
        public Image[] coins;
        public Image[] shields;
        public TextMeshProUGUI stackSizeText;
        public HoverOverController hoverOverController;

        private readonly Color _coinEnabledColor = new Color(217 / 255f, 206 / 255f, 36 / 255f);
        private readonly Color _disabledColor = new Color(114 / 255f, 114 / 255f, 114 / 255f);
        private readonly Color _shieldEnabledColor = new Color(0 / 255f, 168 / 255f, 255 / 255f);
        private Action _actionBlueprint;
        private Sector.ActionPlacementResult _apr;
        private ColorBlock _deactivatedColorBlock;
        private Button _deployButton;

        private GameManager _gm;
        private ColorBlock _normalColorBlock;

        private void Start() {
            this._gm = GameManager.I;
            this._deployButton = this.GetComponent<Button>();
            this._deactivatedColorBlock = this._normalColorBlock = this._deployButton.colors;
            this._deactivatedColorBlock.disabledColor = new Color(140f / 255f, 0f / 255f, 0f / 255f);
            this._actionBlueprint = GameManager.I.AvailableActions.GetBlueprintById(this.actionId);

            this.buildTimeText.text =
                GameTime.FormatTime(this._actionBlueprint?.BuildTime ?? 0, GameTime.TimeFormat.AsDuration);

            if (this.stackSizeText is null) return;

            // ReSharper disable once Unity.NoNullPropagation
            string text = this._actionBlueprint?.AreaUsage + "m²";

            if (this.stackSizeText.text != text) this.stackSizeText.text = text;

            this.EnableIcons(this.coins, this._actionBlueprint?.Cost ?? 0, this._coinEnabledColor);
            // FIXME (at some point): Shields cannot be automatically adjusted because their configuration values are
            // not between 0 and 5.
            // this.EnableIcons(this.shields, this._actionBlueprint?.Protection ?? 0, this._shieldEnabledColor);
        }

        public void BuildAction() {
            var gm = GameManager.I;

            var action = gm.AvailableActions.GetById(this.actionId);
            if (action is null) throw new NullReferenceException($"Action with the id {this.actionId} was null.");

            var selectedSectorRepresentation =
                this.uiController.gameController.MapRepresentation.SelectedSector;
            if (selectedSectorRepresentation is null)
                throw new NullReferenceException("Action cannot be placed when no sector is selected");

            var r = selectedSectorRepresentation.Sector.PlaceAction(action);

            if (r == Sector.ActionPlacementResult.Ok) BankAccount.Instance.Buy(action, action.AreaUsage);
        }

        // Function that is called by hovering over the Action Icon
        public void OnMouseVisit() {
            this._apr = this.CheckActionInSector();

            if (this._gm.CurrentLevel != null && this._apr == Sector.ActionPlacementResult.Ok) {
                this._deployButton.interactable = true;
                this._deployButton.colors = this._normalColorBlock;
                this.actionDeploymentCanvas.SetActive(true);
            } else {
                this._deployButton.interactable = false;
                this._deployButton.colors = this._deactivatedColorBlock;
                this.hoverOverController.ShowHoverOverForResult(this._apr);
            }
        }

        public void OnMouseLeave() {
            this._deployButton.interactable = false;
            this._deployButton.colors = this._normalColorBlock;
            this.actionDeploymentCanvas.SetActive(false);
            this.hoverOverController.HideHoverOvers(this._apr);
        }

        // Checks whether action is available in the Sector, it it is Stackable and if the Countdown Time has elapsed.
        private Sector.ActionPlacementResult CheckActionInSector() {
            var currentSector = this.uiController.gameController.MapRepresentation.SelectedSector.Sector;

            return currentSector.CheckActionPlacement(this._actionBlueprint, true);
        }

        private void EnableIcons(IReadOnlyList<Image> icons, int count, Color enabledColor) {
            if (count > icons.Count) count = icons.Count;

            for (var i = 0; i < icons.Count; i++)
                icons[i].color = i < count ? enabledColor : this._disabledColor;
        }
    }
}