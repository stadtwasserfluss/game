using Artefacts.Scripts.UI.Map;
using UnityEngine;

namespace Artefacts.Scripts.UI.Camera {
    public class CameraHop : MonoBehaviour {
        public Vector2[] defaultPositionsOverSectors;
        public MapRepresentation mapRepresentation;

        private Transform _cameraTransform;

        private void Start() {
            this._cameraTransform = this.transform;
        }

        public void MoveCameraOverSector(int id) {
            this._cameraTransform.position = new Vector3(
                this.defaultPositionsOverSectors[id].x,
                this._cameraTransform.position.y,
                this.defaultPositionsOverSectors[id].y
            );

            var sectorRepresentation = this.mapRepresentation.SectorById(id);

            if (!sectorRepresentation.Sector.IsActive) return;

            if (sectorRepresentation.IsSelected)
                sectorRepresentation.Deselect();
            else
                sectorRepresentation.Select();
        }
    }
}