﻿using Artefacts.Scripts.API.Gameplay;
using Artefacts.Scripts.UI.Map;
using UnityEngine;

namespace Artefacts.Scripts.UI.Camera {
    public class MinimapRayCast : MonoBehaviour {
        public UnityEngine.Camera minimapCamera;
        public CameraHop mainCameraHop;
        public AudioSource clickAccepted;
        public AudioSource clickRejected;

        public void MoveCamera(RectTransform minimapRectTransform) {
            if (!GameManager.I.CurrentLevel.MinimapIsClickable) {
                this.clickRejected.Play();
                return;
            }

            var sizeDelta = minimapRectTransform.sizeDelta;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(minimapRectTransform, Input.mousePosition,
                                                                    null, out var relativeClickPosition);
            relativeClickPosition /= sizeDelta;

            var ray = this.minimapCamera.ViewportPointToRay(relativeClickPosition);

            if (!Physics.Raycast(ray, out var hit)) {
                this.clickRejected.Play();
                return;
            }

            // Hits the Sector Mesh
            var hitObject = hit.transform.parent.gameObject;
            this.mainCameraHop.MoveCameraOverSector(hitObject.GetComponent<SectorRepresentation>().id);

            this.clickAccepted.Play();
        }
    }
}