﻿using System;
using Artefacts.Scripts.API.Gameplay;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Artefacts.Scripts.UI.Camera {
    public class ZoomMouseWheel : MonoBehaviour {
        public int zoomInputSpeed = 16;
        public float zoomInThreshold = -50;
        public float zoomOutThreshold = 130;
        private Transform _cameraController;
        private float _currentZoom, _targetZoom;
        private GameManager _gm;
        private Vector3 _relativeUp, _relativeBackward;

        /// Start is called before the first frame update
        private void Start() {
            this._cameraController = this.GetComponent<Transform>();
            this._relativeUp = this._cameraController.up;
            this._relativeBackward = -1 * this._cameraController.forward;
            this._currentZoom = 0;
            this._targetZoom = this._currentZoom;
            this._gm = GameManager.I;
        }

        /// Zooming in / out by moving the camera up+back/down+forward
        private void Update() {
            if (this._gm.CurrentLevel?.CameraIsLocked == true || EventSystem.current.IsPointerOverGameObject()) return;

            if (Input.GetAxis("Mouse ScrollWheel") > 0) // Zoom in
                this._targetZoom = Math.Max(this.zoomInThreshold, this._targetZoom - this.zoomInputSpeed);
            else if (Input.GetAxis("Mouse ScrollWheel") < 0) // Zoom out
                this._targetZoom = Math.Min(this.zoomOutThreshold, this._targetZoom + this.zoomInputSpeed);

            float delta = this._targetZoom - this._currentZoom;
            float zoomFactor = delta / 2;
            this._cameraController.position += (this._relativeUp + this._relativeBackward) * zoomFactor;
            this._currentZoom += zoomFactor;
        }
    }
}