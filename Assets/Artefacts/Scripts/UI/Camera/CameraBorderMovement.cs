﻿using Artefacts.Scripts.API.Gameplay;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Artefacts.Scripts.UI.Camera {
    public class CameraBorderMovement : MonoBehaviour {
        [Range(0.01f, 0.99f)] public float deadZoneHeight = 0.05f;
        [Range(0.01f, 0.99f)] public float deadZoneWidth = 0.05f;

        [Range(0.0f, 15.0f)] public float dragSpeed = 7f;
        private GameManager _gm;
        private Vector3 _mousePosition;

        private void Start() {
            this._gm = GameManager.I;
        }

        private void Update() {
            if (this._gm.CurrentLevel?.CameraIsLocked == true || EventSystem.current.IsPointerOverGameObject()) return;

            this._mousePosition = Input.mousePosition;

            float deadVerticalStart = (1f - this.deadZoneHeight) / 2f;
            float deadVerticalEnd = 1f - deadVerticalStart;
            float deadHorizontalStart = (1f - this.deadZoneWidth) / 2f;
            float deadHorizontalEnd = 1f - deadHorizontalStart;

            float[] screenWithThirds = {0, Screen.width * deadVerticalStart, Screen.width * deadVerticalEnd};
            float[] screenHeightThirds = {Screen.height * deadHorizontalEnd, Screen.height * deadHorizontalStart, 0};

            float mouseX = this._mousePosition.x;
            float mouseY = this._mousePosition.y;
            var forward = this.transform.forward;
            var backward = -forward;
            var right = this.transform.right;
            var left = -right;
            var direction = new Vector3();

            // Mouse is in the left vertical third of the screen
            if (mouseX < screenWithThirds[1]) direction += left;

            // Mouse is in the middle vertical third of the screen
            if (mouseX >= screenWithThirds[1] && mouseX <= screenWithThirds[2]) {
                // Dead zone
            }

            // Mouse is in the right vertical third of the screen
            if (mouseX > screenWithThirds[2]) direction += right;

            // Mouse is in the top horizontal third of the screen
            if (mouseY > screenHeightThirds[0]) direction += forward;

            // Mouse is in the middle horizontal third of the screen
            if (mouseY <= screenHeightThirds[0] && mouseY >= screenHeightThirds[1]) {
                // Dead zone
            }

            // Mouse is in the bottom horizontal third of the screen
            if (mouseY < screenHeightThirds[1]) direction += backward;

            direction.Normalize();
            direction *= this.dragSpeed;

            this.transform.position += direction;
        }
    }
}