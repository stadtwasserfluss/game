﻿using Artefacts.Scripts.API.Gameplay;
using UnityEngine;

namespace Artefacts.Scripts.UI.Camera {
    public class ScrollAndPinch : MonoBehaviour {
        //this variable is defined in the Inspector
        public float speed = 0.1f;

        public Vector3 minPos = new Vector3(-5f, 4.9f, -7f);
        public Vector3 maxPos = new Vector3(5f, 5f, 2f);

        private void Update() {
            if (GameManager.I.CurrentLevel.CameraIsLocked)
                return;

            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
                var touchDeltaPosition = Input.GetTouch(0).deltaPosition;
                this.transform.Translate(-touchDeltaPosition.x * this.speed,
                                         0,
                                         -touchDeltaPosition.y * this.speed);

                this.transform.position = new Vector3(
                    Mathf.Clamp(this.transform.position.x, this.minPos.x, this.maxPos.x),
                    Mathf.Clamp(this.transform.position.y, this.minPos.y, this.maxPos.y),
                    Mathf.Clamp(this.transform.position.z, this.minPos.z, this.maxPos.z)
                );
            }

            if (Input.touchCount != 2) return;

            var touchZero = Input.GetTouch(0);
            var touchOne = Input.GetTouch(1);

            var touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            var touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            UnityEngine.Camera.main.fieldOfView += deltaMagnitudeDiff * this.speed;

            UnityEngine.Camera.main.fieldOfView = Mathf.Clamp(UnityEngine.Camera.main.fieldOfView, 15.0f, 70.0f);
        }
    }
}