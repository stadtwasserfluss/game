﻿using UnityEngine;

namespace Artefacts.Scripts.UI.Camera {
    public class CameraFacingBillboard : MonoBehaviour {
        public UnityEngine.Camera targetCamera;

        // Set Reference to Camera
        private void Start() {
            this.targetCamera = GameObject.Find("Camera").GetComponent<UnityEngine.Camera>();
        }

        //Orient the camera after all movement is completed this frame to avoid jittering
        private void LateUpdate() {
            this.transform.LookAt(this.transform.position + this.targetCamera.transform.rotation * Vector3.forward,
                                  this.targetCamera.transform.rotation * Vector3.up);
        }
    }
}