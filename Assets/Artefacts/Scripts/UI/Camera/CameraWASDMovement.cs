using Artefacts.Scripts.API.Gameplay;
using UnityEngine;

namespace Artefacts.Scripts.UI.Camera {
    // ReSharper disable once InconsistentNaming
    public class CameraWASDMovement : MonoBehaviour {
        // this is a location that is set to the middle of my world, it will be the center of your circle.
        public Vector3 boundaryCenter;

        // this is the range you want the player to move without restriction
        public float radius = 20f;

        // get the player transform, or w/e object you want to limit in a circle
        public Transform cameraController;

        // Basic camera movement speed
        public int movementSpeed = 1000;

        private GameManager _gm;
        private float _rotationHorizontal;

        private void Start() {
            this._gm = GameManager.I;
            // Inherit camera's horizontal orientation from editor.
            this._rotationHorizontal = this.transform.eulerAngles.y;
        }

        private void Update() {
            // the distance from player current position to the circleCenter
            float dist = Vector3.Distance(this.cameraController.position, this.boundaryCenter);

            Application.targetFrameRate = 60;

            if (dist > this.radius) {
                var fromOrigintoObject = this.cameraController.position - this.boundaryCenter;
                fromOrigintoObject *= this.radius / dist;
                this.cameraController.position = this.boundaryCenter + fromOrigintoObject;
                this.transform.position = this.cameraController.position;
            }

            if (this._gm.CurrentLevel?.CameraIsLocked == true) return;

            this.transform.localRotation = Quaternion.AngleAxis(this._rotationHorizontal, Vector3.up);

            float movementSpeedNormalized = this.movementSpeed * Time.deltaTime / 10;

            this.transform.position += this.transform.forward * (Input.GetAxis("Vertical") * movementSpeedNormalized);
            this.transform.position += this.transform.right * (Input.GetAxis("Horizontal") * movementSpeedNormalized);
        }
    }
}