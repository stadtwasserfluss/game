using UnityEngine;
using UnityEngine.UI;

namespace Artefacts.Scripts.UI.Statistics {
    public class ActionStatusIcon : MonoBehaviour {
        private static Color _activeColor;
        private static Color _inactiveColor;

        public string actionId;
        private Image _icon;

        public bool Activated { get; set; }

        private void Start() {
            this._icon = this.GetComponent<Image>();
            ActionStatusIcon._inactiveColor = this._icon.color;
            ActionStatusIcon._activeColor = new Color(138f / 255f, 215f / 255f, 255f / 255f);
        }

        private void Update() {
            if (!this.Activated && this._icon.color != ActionStatusIcon._inactiveColor)
                this._icon.color = ActionStatusIcon._inactiveColor;

            if (this.Activated && this._icon.color != ActionStatusIcon._activeColor)
                this._icon.color = ActionStatusIcon._activeColor;
        }
    }
}