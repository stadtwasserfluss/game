using Artefacts.Scripts.API.Gameplay;
using TMPro;
using UnityEngine;

namespace Artefacts.Scripts.UI.Statistics {
    public class MoneyDisplay : MonoBehaviour {
        private BankAccount _ba;
        private int _initialDamagePoints;
        private TextMeshProUGUI _textDisplay;

        // Start is called before the first frame update
        private void Start() {
            this._textDisplay = this.GetComponent<TextMeshProUGUI>();
            this._ba = BankAccount.Instance;
        }

        // Update is called once per frame
        private void Update() {
            this._textDisplay.text = this._ba.Saldo.ToString();
        }
    }
}