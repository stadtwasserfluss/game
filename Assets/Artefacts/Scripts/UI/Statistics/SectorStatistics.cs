using System.Collections.Generic;
using Artefacts.Scripts.UI.Map;
using TMPro;
using UnityEngine;

namespace Artefacts.Scripts.UI.Statistics {
    public class SectorStatistics : MonoBehaviour {
        public TextMeshProUGUI availableGroundSpace;
        public TextMeshProUGUI availableRoofSpace;

        public List<ActionStatusIcon> icons;
        public TextMeshProUGUI sectorNumber;
        public UiController uiController;
        private SectorRepresentation _selectedSector;
        private Canvas _statisticsCanvas;

        private void Awake() {
            this._statisticsCanvas = this.GetComponent<Canvas>();
        }

        private void Update() {
            this._selectedSector = this.uiController.gameController.MapRepresentation.SelectedSector;

            // ReSharper disable once Unity.PerformanceCriticalCodeNullComparison
            this._statisticsCanvas.enabled = this._selectedSector != null;

            if (!this._statisticsCanvas.enabled) return;

            this.sectorNumber.text = this._selectedSector.Sector.Id.ToString();
            this.availableGroundSpace.text = this._selectedSector.Sector.AvailableGroundArea + " m²";
            this.availableRoofSpace.text = this._selectedSector.Sector.AvailableRoofArea + " m²";

            foreach (var icon in this.icons)
                icon.Activated = this._selectedSector.Sector.CountEffectiveImplementationsOfAction(icon.actionId) > 0;
        }
    }
}