﻿using Artefacts.Scripts.API.Gameplay;
using UnityEngine;
using UnityEngine.UI;

namespace Artefacts.Scripts.UI.Statistics {
    public class PublicAwarenessDisplay : MonoBehaviour, IGameTimeComponent {
        public Slider pASlider;
        private GameManager _gm;
        private GameTime _gt;
        private int _timeStepCounter;

        // Setup references
        private void Start() {
            this._gm = GameManager.I;
            this._gt = GameTime.I;
            this._gt.AddComponent(this);

            this._timeStepCounter = this._gm.CurrentLevel.PublicAwarenessConfiguration.Display.TimeStepsForDecrease;
        }

        // Update value of Public Awareness Bar
        private void Update() {
            if (this._gm.CurrentLevel != null) this.pASlider.value = this._gm.CurrentLevel.PublicAwareness;
        }

        public void BeforeTimeStep(long now) { }

        public void OnTimeStep(long now) { }

        // Public Awareness decreses with time
        public void AfterTimeStep(long now) {
            if (this._timeStepCounter == 0) {
                this._gm.CurrentLevel.DecreasePublicAwarenessBy(
                    this._gm.CurrentLevel.PublicAwarenessConfiguration.Display.DecreaseWithTime);
                this._timeStepCounter = this._gm.CurrentLevel.PublicAwarenessConfiguration.Display.TimeStepsForDecrease;
            } else {
                this._timeStepCounter--;
            }
        }
    }
}