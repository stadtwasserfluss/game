﻿using System.Globalization;
using Artefacts.Scripts.API.Gameplay;
using TMPro;
using UnityEngine;

namespace Artefacts.Scripts.UI.Statistics {
    public class TotalProtectionDisplay : MonoBehaviour {
        private GameManager _gm;
        private TextMeshProUGUI _textDisplay;

        // Start is called before the first frame update
        private void Start() {
            this._textDisplay = this.GetComponent<TextMeshProUGUI>();
            this._gm = GameManager.I;
        }

        // Update is called once per frame
        private void Update() {
            this._textDisplay.text = this._gm.Map.TotalProtection.ToString(CultureInfo.CurrentUICulture);
        }
    }
}