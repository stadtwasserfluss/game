using Artefacts.Scripts.API.Gameplay;
using TMPro;
using UnityEngine;

namespace Artefacts.Scripts.UI.Statistics {
    public class TimeDisplay : MonoBehaviour {
        private GameTime _gt;
        private TextMeshProUGUI _timeText;

        private void Awake() {
            this._gt = GameTime.I;
        }

        // Start is called before the first frame update
        private void Start() {
            this._timeText = this.GetComponent<TextMeshProUGUI>();
        }

        // Update is called once per frame
        private void Update() {
            this._timeText.text = EventManager.I.IsInPreparationPhase ? this._gt.FormatAsHours : this._gt.FormatAsDays;
        }
    }
}