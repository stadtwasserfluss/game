﻿using System;
using System.Linq;
using Artefacts.Scripts.API.Gameplay;
using Artefacts.Scripts.API.Notification;
using UnityEngine;

namespace Artefacts.Scripts.UI {
    public class RainControl : MonoBehaviour, IGameTimeComponent {
        public int burstEmission = 160;
        public int maxEmission = 120;
        public int minEmission = 10;
        public int startAtTimeLeft = 8;
        public AudioSource rainSound;
        private ParticleSystem _rainSystem;

        private void Start() {
            GameTime.I.AddComponent(this);
            this._rainSystem = this.GetComponent<ParticleSystem>();
        }

        public void BeforeTimeStep(long now) { }

        public void OnTimeStep(long now) {
            var gm = GameManager.I;
            var events = gm.CurrentEvents;
            var notifications =
                gm.CurrentNotifications.OfType<CatastropheNotification>().ToList();

            var affectingEvent = events.FirstOrDefault(e => e.IsAffecting);
            var notification =
                notifications.FirstOrDefault(c => c.DurationLeft <= this.startAtTimeLeft);

            var emission = this._rainSystem.emission;

            if (notification != null) {
                float interpolatedEmission = this.minEmission +
                                             (float) (this.startAtTimeLeft - notification.DurationLeft) /
                                             this.startAtTimeLeft * (this.maxEmission - this.minEmission);

                emission.rateOverTime = interpolatedEmission;
                this.rainSound.volume = Math.Max(0.3f, Math.Min(interpolatedEmission / this.maxEmission, 1f));
            }

            if (affectingEvent != null) {
                emission.rateOverTime = this.burstEmission;
                this.rainSound.volume = 1;
            }

            if (notification != null || affectingEvent != null) {
                if (!this._rainSystem.isPlaying)
                    this._rainSystem.Play();

                if (!this.rainSound.isPlaying)
                    this.rainSound.Play();
            } else {
                this._rainSystem.Stop();
                this.rainSound.Stop();
            }
        }

        public void AfterTimeStep(long now) { }
    }
}