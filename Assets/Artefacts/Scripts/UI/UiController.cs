using System.Linq;
using Artefacts.Scripts.API.Gameplay;
using Artefacts.Scripts.API.Notification;
using Artefacts.Scripts.UI.Buttons;
using Artefacts.Scripts.UI.Communications;
using UnityEngine;

namespace Artefacts.Scripts.UI {
    public class UiController : MonoBehaviour, IGameTimeComponent {
        public Canvas farewellCanvas;
        public GameController gameController;
        public GameObject exitMenu;
        public GameControls gameControls;
        public PauseButtonUpdater pauseButtonUpdater;

        private CommunicationsWindow _communicationsWindow;
        private bool _paused;

        private void Awake() {
            GameTime.I.AddComponent(this);
        }

        private void Start() {
            this._communicationsWindow = this.GetComponentInChildren<CommunicationsWindow>();
        }

        private void Update() {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                bool isActive = this.exitMenu.activeSelf;

                this.exitMenu.SetActive(!isActive);
                this.gameControls.QuitPause();
            }

            if (!Input.GetKeyDown(KeyCode.Space)) return;

            if (!this._paused) {
                this._paused = true;
                this.pauseButtonUpdater.PauseButtonClick();
            } else {
                this._paused = false;
                this.pauseButtonUpdater.PlayButtonClick();
            }
        }

        public void BeforeTimeStep(long now) { }

        public void OnTimeStep(long now) { }

        public void AfterTimeStep(long now) {
            if (GameManager.I.CurrentNotifications.OfType<CatastrophePassedNotification>().Any())
                this._communicationsWindow.Show(1);

            if (GameManager.I.GameState == GameManager.GameStates.Ended)
                this.farewellCanvas.gameObject.SetActive(true);
        }

        public void NotifyStory(string eventString) {
            GameManager.I.CurrentStory?.Notify(eventString);
        }
    }
}