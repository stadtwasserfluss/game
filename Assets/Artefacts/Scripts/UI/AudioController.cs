using UnityEngine;
using UnityEngine.UI;

namespace Artefacts.Scripts.UI {
    public class AudioController : MonoBehaviour {
        public Button enableSoundButton;
        public Button disableSoundButton;

        private void Start() {
            this.enableSoundButton.onClick.AddListener(AudioController.EnableAudio);
            this.disableSoundButton.onClick.AddListener(AudioController.DisableAudio);
        }

        private void Update() {
            bool audioEnabled = PlayerPrefs.GetInt("audio", 1) == 1;
            AudioListener.volume = audioEnabled ? 1 : 0;
            this.enableSoundButton.gameObject.SetActive(!audioEnabled);
            this.disableSoundButton.gameObject.SetActive(audioEnabled);
        }

        private static void EnableAudio() {
            PlayerPrefs.SetInt("audio", 1);
            PlayerPrefs.Save();
        }

        private static void DisableAudio() {
            PlayerPrefs.SetInt("audio", 0);
            PlayerPrefs.Save();
        }
    }
}