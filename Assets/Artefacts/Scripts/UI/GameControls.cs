using Artefacts.Scripts.API;
using Artefacts.Scripts.API.Gameplay;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Artefacts.Scripts.UI {
    public class GameControls : MonoBehaviour {
        private static bool _pausedByPlayer;

        public void Fastfoward() {
            GameTime.I.SetSpeedup(GameManager.I.CurrentLevel.FastForwardTimeFactor);
        }

        public void NormalSpeed() {
            GameTime.I.SetSpeedup(1);
        }

        // Called, when Game is resumed after clicking Exit Button
        public void ResumeGame() {
            if (!GameControls._pausedByPlayer) GameControls.UnpauseGame();
        }

        // Called, when Exit Button is clicked
        public void QuitPause() {
            GameTime.I.Pause();
        }

        public void PauseGame() {
            GameControls._pausedByPlayer = true;
            GameTime.I.Pause();
        }

        public void QuitGame() {
            Application.Quit();
        }

        public void ChangeScene(string sceneName) {
            ApiController.ResetLazySingletons();
            SceneManager.LoadScene(sceneName);
        }

        private static void UnpauseGame() {
            GameControls._pausedByPlayer = false;
            GameTime.I.Unpause();
        }
    }
}