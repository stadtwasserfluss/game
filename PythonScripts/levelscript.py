import os
import yaml

directoryfrom = ".\\"
directoryto = ".\\"

for filename in os.listdir(directoryfrom):
    if filename.endswith(".yaml"):

        code = ""
        level = ""

        with open(filename, 'r', encoding='utf8') as stream:
            try:
                data = yaml.safe_load(stream)
                level = int(data['Id'])
                code += """#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Gameplay;

namespace Artefacts.Scripts.API.Yaml.WebGl.Levels {
    public class Level""" + "{:02d}".format(level) + """ : Level {
        public Level""" + "{:02d}".format(level) + """() {
            this.Id = """ + str(level) + """;
            this.Next = """ + str(data['Next']) + """;
            this.EventAt = """ + str(data['EventAt']) + """;
            this.EventDuration = """ + str(data['EventDuration']) + """;
            this.PreparationTime = """ + str(data['PreparationTime']) + """;
            this.FastForwardTimeFactor = """ + str(data['FastForwardTimeFactor']) + """f;
            this.AdditionalMoney = """ + str(data['AdditionalMoney']) + """;
            this.AffectedSectors = new[] { """
                for value in data['AffectedSectors']:
                    code = code + str(value) + ","
                code += """};
            this.ActiveSectors = new[] {"""
                for value in data['ActiveSectors']:
                    code = code + str(value) + ","
                code += """};
            this.CameraIsLocked = """ + str(data['CameraIsLocked']).lower() + """;
            this.MinimapIsClickable = """ + str(data['MinimapIsClickable']).lower() + """;
            this.EnabledActions = new[] {"""
                for value in data['EnabledActions']:
                    code = code + "\"" + str(value) + "\","
                code += """};
            this.RainIndex = """ + str(data['RainIndex']) + """;
            this.PreventedDamageRatingThresholds = new[] {"""
                for value in data['PreventedDamageRatingThresholds']:
                    code = code + str(value) + "f,"
                code += """};
            this.PublicAwarenessConfiguration = new Level""" + "{:02d}".format(level) + """PaConfiguration();
            this.BoostFromPrivateActions =""" + str(data['BoostFromPrivateActions']) + """f;
            this.RainIndexDistribution = new Dictionary<int, float> {"""
                for key, value in data['RainIndexDistribution'].items():
                    code += "{" + str(key) + "," + str(value) + "f},"
                code += """};
            this.SectorImportance = new Dictionary<int, float> {"""
                for key, value in data['SectorImportance'].items():
                    code += "{" + str(key) + "," + str(value) + "f},"
                code += """};
            }

        private class Level""" + "{:02d}".format(level) + """PaConfiguration : PaConfiguration {
            public Level""" + "{:02d}".format(level) + """PaConfiguration() {
                this.InitialPublicAwareness = """ + str(data['PublicAwarenessConfiguration']['InitialPublicAwareness']) + """f;
                this.Display = new Level""" + "{:02d}".format(level) + """PaConfigurationPaDisplay();
                this.Button = new Level""" + "{:02d}".format(level) + """PaConfigurationPaButton();
                this.QuizBonus = """ + str(data['PublicAwarenessConfiguration']['QuizBonus']) + """f;
            }
    
            private class Level""" + "{:02d}".format(level) + """PaConfigurationPaDisplay : PaDisplay {
                public Level""" + "{:02d}".format(level) + """PaConfigurationPaDisplay() {
                    this.DecreaseWithTime = """ + str(data['PublicAwarenessConfiguration']['Display']['DecreaseWithTime']) + """f;
                    this.TimeStepsForDecrease = """ + str(data['PublicAwarenessConfiguration']['Display']['TimeStepsForDecrease']) + """;
                }
            }
    
            private class Level""" + "{:02d}".format(level) + """PaConfigurationPaButton : PaButton {
                public Level""" + "{:02d}".format(level) + """PaConfigurationPaButton() {
                    this.OnClickRaiseBy = """ + str(data['PublicAwarenessConfiguration']['Button']['OnClickRaiseBy']) + """f;
                    this.OnClickPenaltyDecreaseBy = """ + str(data['PublicAwarenessConfiguration']['Button']['OnClickPenaltyDecreaseBy']) + """f;
                    this.PenaltyTimeSteps = """ + str(data['PublicAwarenessConfiguration']['Button']['PenaltyTimeSteps']) + """;
                }
            }
        }
    }
}
#endif"""

            except yaml.YAMLError as exc:
                print(exc)

        with open("Level" + "{:02d}".format(level) + ".cs", 'w', encoding="utf8") as file:
            file.write(code)
            file.close()
