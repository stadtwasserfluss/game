import yaml
import os

directoryfrom = ".\\"
directoryto = ".\\"

for filename in os.listdir(directoryfrom):
    if filename.endswith(".yaml"):

        storyId = ""
        code = ""

        with open(filename, 'r', encoding='utf8') as stream:
            try:
                data = yaml.safe_load(stream)

                storyId = int(data['Level'])
                code += """#if UNITY_WEBGL
using System.Collections.Generic;
using Artefacts.Scripts.API.Communication;
using Artefacts.Scripts.API.Communication.Quiz;

namespace Artefacts.Scripts.API.Yaml.WebGl.Story {
    public class Level""" + "{:02d}".format(data['Level']) + """Story : Communication.Story {
        public Level""" + "{:02d}".format(data['Level']) + """Story() {
            this.Level = """ + str(data['Level']) + """;
            this.Messages = new List<Message>{"""
                for message in data['Messages']:
                    if message['When'] is None:
                        message['When'] = "null"
                    else:
                        message['When'] = "\"" + message['When'] + "\""

                    if len(message) == 5:
                        code += """
                new Message {
                    When = """ + message['When'] + """,
                    Delay = """ + str(message['Delay']) + """,
                    From = \"""" + message['From'] + """\",
                    Text = @\"""" + message['Text'].replace('"', '""') + """\",
                    Pause = """ + str(message['Pause']).lower() + """,
                },
                        """

                    else:
                        code += """
                new Message {
                    When = """ + str(message['When']) + """,
                    Delay = """ + str(message['Delay']) + """,
                    From = \"""" + message['From'] + """\",
                    Text = @\"""" + message['Text'].replace('"', '""') + """\",
                },
                        """

                code += """
            };
            this.Emails = new List<Email> {
                """
                for email in data['Emails']:
                    if email['When'] is None:
                        email['When'] = "null"
                    else:
                        email['When'] = "\"" + email['When'] + "\""
                    code += """
                new Email {
                    Id = """ + str(email['Id']) + """,
                    When = """ + str(email['When']) + """,
                    Delay = """ + str(email['Delay']) + """,
                    From = \"""" + email['From'] + """\",
                    FromAdress = \"""" + email['FromAdress'] + """\",
                    Subject = " """ + email['Subject'] + """\",
                    Text = @\"""" + email['Text'].replace('"', '""') + """\",
                },"""

                code += """
            };
            this.Questions = new List<Question> {
                """

                for question in data['Questions']:
                    if question['When'] is None:
                        question['When'] = "null"
                    else:
                        question['When'] = "\"" + question['When'] + "\""
                    code += """
                new Question {
                    Id = """ + str(question['Id']) + """,
                    When = """ + str(question['When']) + """,
                    Text = @\"""" + question['Text'].replace('"', '""') + """\",
                    Answers = new List<Question.Answer> {"""
                    for answer in question['Answers']:
                        code += """
                        new Question.Answer {
                            Text = @\"""" + answer['Text'].replace('"', '""') + """\",
                            IsCorrect = """ + ("true" if answer['IsCorrect'] else "false") + """
                        },"""
                    code += """
                    },
                },"""

                code += """
            };
        }
    }
}
#endif"""

            except yaml.YAMLError as exc:
                print(exc)

        with open("Level" + "{:02d}".format(storyId) + "Story.cs", 'w', encoding="utf8") as file:
            file.write(code)
            file.close()
