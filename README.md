# StadtWasserFluss

### Starkregenrisikomanagement als Serious Game

<p align="center">
  <img src="Game_Screenshot.png" height="300"><br>
  <a href="https://www.stadtwasserfluss.de/"><b>Zum Spiel!</b></a>
</p>

## Das Spiel

Im Projekt StadtWasserFluss hat das Lehr- und Forschungsgebiet Ingenieurhydrologie (LFI) und das IT Center (ITC) in Kooperation mit dem Wasserverband Eifel-Rur (WVER) ein Konzept entwickelt in dem Studierende, Starkregenbetroffene und Interessierte an das Thema Starkregen herangeführt werden. Der Spieler übernimmt eine Managementposition im Amt für Starkregen und Sturzfluten und kann im Verlauf des Spieles verschiedene Maßnahmen ergreifen um den Schaden zu verringern, der bei Starkregenereignissen auftritt. Das Spiel wurde für Windows, Android und als Web-Version veröffentlicht und lässt sich kostenlos spielen.

### Konzept

Der Spieler wird in die Rolle eines Starkregenrisikomanagement-Beauftragten gesetzt und erhält von seinem Vorgesetzten und Kollegen Hinweise was zu tun ist. Als Spieler kann er verschiedene Maßnahmen ergreifen, die die Stadt Aachen besser vor zukünftigen Starkregen-Ereignissen schützt. Zusätzlich muss der Spieler Fragen beantworten und kann dadurch ein höheres Bewusstsein der Bevölkerung für das Thema erreichen.

In verschiedenen Leveln muss der Spieler erst einzelne Bereich der Stadt beschützen. Im finalen Level ist er für den ganzen Stadtkern der Stadt Aachen zuständig.

### Starkregen

Starkregen ist ein Phänomen, welches klimabedingt in Zukunft immer häufiger auftreten und Schäden verursachen wird. Durch die Verschiebung der Regenereignisse von gleichmäßig über das Jahr verteilten Regenmengen, hin zu längeren Dürreperioden und stärkeren Regenereignissen muss in Zukunft das Wissen über Schutzmaßnahmen intensiver aufgebaut, verstetigt, erweitert und weiterverbreitet werden.

Das entstandene Spiel und die weiteren Lernmaterialien sollen einen spielenden Einstieg in das Thema bieten und zur Schulung in der Aus- und Weiterbildung von Studierenden, Ehrenämtlern und Starkregenbetroffenen dienen aber auch Interessierten die Möglichhkeit geben sich mit dem Thema zu beschäftigen.

### Konfiguration

Das Spiel wurde so entwickelt, dass eine einfache Konfiguration der Level möglich ist. Dadurch sollen Lehrende eine einfache Möglichkeit erhalten, das Spiel in anderen Lehrveranstaltungen einzusetzen. In den Verzeichnissen [`Assets/StreamingAssets/Data`](Assets/StreamingAssets/Data) liegen die Konfigurationsdateien des Spiels, über die u.a. die Level und Geschichte des Spiels verändert werden kann.

### Lizenz

<a href="https://creativecommons.org/licenses/by-sa/3.0/de/"><img src="https://files.lfi.rwth-aachen.de/stadtwasserfluss/cc-by-sa.svg" height="40"></a>

Das Spiel ist als Open Education Resource (OER) veröffentlicht und fällt unter die Creative Commons Lizenz [`CC BY-SA 3.0 DE`](https://creativecommons.org/licenses/by-sa/3.0/de/). Davon ausgenommen sind die Lernposter im Verzeichnis [`Assets/MyAssets/MySprites/Learning Posters`](Assets/Artfacts/Sprites/Posters), da dort auch Inhalte Dritter zitiert werden.

Der Quellcode des Spieles ist unter der [GPL 3](LICENSE) veröffentlicht.

## Projektpartner

<a href="https://www.lfi.rwth-aachen.de/"><img src="https://files.lfi.rwth-aachen.de/stadtwasserfluss/lfi.png" height="90"></a>

<a href="https://www.itc.rwth-aachen.de/"><img src="https://files.lfi.rwth-aachen.de/stadtwasserfluss/itc.png" height="90"></a>

<a href="https://wver.de/"><img src="https://files.lfi.rwth-aachen.de/stadtwasserfluss/wver.png" height="90"></a>

## Föderung

Gefördert vom [Bundesministerium für Umwelt, Naturschutz und nukleare Sicherheit](https://www.bmu.de/) aufgrund eines Beschlusses des Deutschen Bundestages

<a href="https://www.bmu.de/"><img src="https://www.stadtwasserfluss.de/wp-content/uploads/2020/02/Logo_Bundesministerium_f%C3%BCr_Umwelt_Naturschutz_und_nukleare_Sicherheit-1-300x136.png" height="90"></a>

**Laufzeit:** 01.01.2019 – 31.12.2020  
**Förderprogramm:** Förderung von Maßnahmen zur Anpassung an die Folgen des Klimawandels  
**Förderschwerpunkt:** Entwicklung von Bildungsmodulen zu Klimawandel und Klimaanpassung  
**Förderkennzeichen:** 67DAS150A, 67DAS150B
